#!/usr/bin/env python
"""
This is the template for a script file. Please change this docstring to reflect
realities.
"""
import argparse
import ConfigParser
import logging
import os
import sys
import time

from squirro_client import ItemUploader, SquirroClient

# Script version. It's recommended to increment this with every change, to make
# debugging easier.
VERSION = '0.9.0'


# Set up logging.
log = logging.getLogger('{0}[{1}]'.format(os.path.basename(sys.argv[0]),
                                          os.getpid()))


def run():
    """Main entry point run by __main__ below. No need to change this usually.
    """
    args = parse_args()
    setup_logging(args)
    config = get_config(args)

    log.info('Starting process (version %s).', VERSION)
    log.debug('Arguments: %r', args)

    squirro_client, item_uploader = get_clients(config)

    # run the application
    try:
        main(args, config, squirro_client, item_uploader)
    except Exception:
        log.exception('Processing error')


def main(args, config, squirro_client=None, item_uploader=None):
    """
    The main method. Any exceptions are caught outside of this method and will
    be handled.

    squirro_client and item_uploader are passed in if the [squirro] section of
    the config file is defined.
    """
    log.warn('This method has been intentionally left almost blank.')
    log.info('Number of bytes (sample config): %d',
             config.getint('example', 'bytes'))
    log.info('And a string config example as well: %s',
             config.get('example', 'greeting'))
    raise Exception('Testing exception handling')


def parse_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--version', action='version', version=VERSION)
    parser.add_argument('--verbose', '-v', action='count',
                        help='Show additional information.')
    parser.add_argument('--log-file', dest='log_file',
                        help='Log file on disk.')
    parser.add_argument('--config-file', dest='config_file',
                        help='Configuration file to read settings from.')

    return parser.parse_args()


def setup_logging(args):
    """Set up logging based on the command line options.
    """
    # Set up logging
    fmt = '%(asctime)s %(name)s %(levelname)-8s %(message)s'
    if args.verbose == 1:
        level = logging.INFO
        logging.getLogger(
            'requests.packages.urllib3.connectionpool').setLevel(logging.WARN)
    elif args.verbose >= 2:
        level = logging.DEBUG
    else:
        # default value
        level = logging.WARN
        logging.getLogger(
            'requests.packages.urllib3.connectionpool').setLevel(logging.WARN)

    # configure the logging system
    if args.log_file:
        out_dir = os.path.dirname(args.log_file)
        if out_dir and not os.path.exists(out_dir):
            os.makedirs(out_dir)
        logging.basicConfig(
            filename=args.log_file, filemode='a', level=level, format=fmt)
    else:
        logging.basicConfig(level=level, format=fmt)

    # Log time in UTC
    logging.Formatter.converter = time.gmtime


def get_config(args):
    """Parse the config file and return a ConfigParser object.

    Always reads the `main.ini` file in the current directory (`main` is
    replaced by the current basename of the script).
    """
    cfg = ConfigParser.SafeConfigParser()

    root, _ = os.path.splitext(__file__)
    files = [root + '.ini']
    if args.config_file:
        files.append(args.config_file)

    log.debug('Reading config files: %r', files)
    cfg.read(files)
    return cfg


def get_clients(config, source_name=None):
    """"Return Squirro clients.

    Source name can be provided as a parameter, when one script needs multiple
    different uploaders.
    """
    if not config.has_section('squirro'):
        return None, None

    cluster = config.get('squirro', 'cluster')
    token = config.get('squirro', 'token')
    project_id = config.get('squirro', 'project_id')
    if not source_name and config.has_option('squirro', 'source_name'):
        source_name = config.get('squirro', 'source_name')

    client = SquirroClient(None, None, cluster=cluster)
    client.authenticate(refresh_token=token)

    uploader = None
    if config.getboolean('squirro', 'uploader'):
        uploader = ItemUploader(
            cluster=cluster,
            token=token,
            project_id=project_id,
            source_name=source_name,
        )

    return client, uploader


# This is run if this script is executed, rather than imported.
if __name__ == '__main__':
    run()
