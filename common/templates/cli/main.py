#!/usr/bin/env python
"""
This is the template for a script file. Please change this docstring to reflect
realities.
"""
import logging
import os
import sys

# Get the helpers (adjust path to point to the `common/lib` folder)
sys.path.append(os.path.join(os.path.dirname(__file__), '../../lib'))
import project_config  # noqa


# Set up logging.
log = logging.getLogger('{0}[{1}]'.format(
    os.path.basename(sys.argv[0]), os.getpid()))


# Configuration keys
# Example merges two config keys from the [example] section with the
# corresponding command line arguments.
# Set to `None` to disable config merging.
CONFIG_KEYS = ['arg1', 'arg2']
CONFIG_SECTION = 'example'


def run():
    """Main entry point run by __main__ below. No need to change this usually.
    """
    args = parse_args()
    project_config.setup_logging(args)
    config = project_config.get_config(args.config_file)

    # Merge with arguments from the config file. All the arguments given can
    # be provided either through the command line or through the config file.
    args = project_config.update_args(config, args, CONFIG_KEYS,
                                      CONFIG_SECTION)

    log.debug('Arguments: %r', args)

    # run the application
    try:
        main(args, config)
    except Exception:
        log.exception('Processing error')


def main(args, config):
    """
    The main method. Any exceptions are caught outside of this method and will
    be handled.
    """
    client = project_config.get_client(config)
    uploader = project_config.get_uploader(config)

    log.warn('This method has been intentionally left almost blank.')
    log.info('Number of bytes (sample config): %d',
             config.getint('example', 'bytes'))
    log.info('And a string config example as well: %s',
             config.get('example', 'greeting'))
    raise Exception('Testing exception handling')


def parse_args():
    """Parse command line arguments."""
    parser = project_config.get_arg_parser()
    parser.add_argument('--arg1',
                        help='The first argument.')
    parser.add_argument('--arg2',
                        help='The second argument.')
    return parser.parse_args()


# This is run if this script is executed, rather than imported.
if __name__ == '__main__':
    run()
