#!/bin/bash
set -e
cd $(dirname $0)
source ../config/config.sh
cd $PROJECT_ROOT

pkg=$1
if [ -z "$pkg" ]; then
    echo "Usage: $0 pkg.tar.gz"
    exit 1
fi

# Create a backup
timestamp=`date +%Y-%m-%d-%H-%M`
backup_dir="backup"
backup_target_file="${backup_dir}/deployment-${timestamp}.tar.gz"
echo "Backup existing deployment to ${backup_target_file}"
if [ ! -d "${backup_dir}" ]; then
    mkdir -p "${backup_dir}"
fi
tar -czvf "${backup_target_file}" --exclude "backup/*.tar.gz" .

# Extract the package
tar -xvf $pkg

# Install the data loader
if [ -d "data/loader" ]; then
    echo
    echo 'Installing data loader'
    (cd data/loader && python setup.py develop)
fi

# Install dependencies
if [ -f requirements.txt ]; then
    echo
    echo 'Installing dependencies from requirements.txt'
    pip install -r requirements.txt
fi

# Upload dashboards
if [ -x dashboard/upload.sh ]; then
    echo
    echo 'Uploading dashboard widgets'
    dashboard/upload.sh
fi
