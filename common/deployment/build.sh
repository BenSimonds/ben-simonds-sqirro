#!/bin/bash
set -e
cd $(dirname $0)
source ../config/config.sh
cd $PROJECT_ROOT

NAME="$(getval package build)"
BUILD_FOLDER=/tmp/$NAME/dist
PACKAGE_FILE="squirro-$NAME-$(date +%Y%m%d-%H%M%S).tar.gz"
PACKAGE="/tmp/$NAME/$PACKAGE_FILE"
S3_PATH="$(getval s3_path build 2>/dev/null || true)"
S3_PATH_FULL="${S3_PATH}${PACKAGE_FILE}"

if [ -z "$NAME" ]; then
    echo 'Specify package name as build.package'
    exit 1
fi

echo "Building package for $NAME"

echo
echo 'Cleaning up'
if [ -e $BUILD_FOLDER ]; then
    rm -rf $BUILD_FOLDER
fi
mkdir -p $BUILD_FOLDER

echo
echo 'Copy project'
pwd
rsync -a ./ ${BUILD_FOLDER}/

if [ -d "$BUILD_FOLDER/data" ] && [ ! -d "$BUILD_FOLDER/data/loader" ]; then
    echo
    echo 'Data loader'
    dataloader_source="$(getval dataloader pkg 2>/dev/null || true)"
    dataloader_branch="$(getval dataloader_branch pkg 2>/dev/null || true)"
    if [ -n "$dataloader_source" ]; then
        echo "Copying from $dataloader_source"
        cp -a $dataloader_source $BUILD_FOLDER/data/loader

        if [ -n "$dataloader_branch" ]; then
            echo "Checking out data loader branch $dataloader_branch"
            (cd $BUILD_FOLDER/data/loader && git fetch && git checkout origin/$dataloader_branch)
        fi
    fi
fi

echo
echo 'Remove unneeded artifacts'
pushd $BUILD_FOLDER >/dev/null
if [ -d .git ]; then
    git clean -d -f -X
fi
find . -name '.git*' | xargs rm -rf {} \;

# Config is removed, so we don't inadvertently overwrite config on the server.
# If some config needs to be deployed, then put it into the `deployment/config`
# folder.
rm -rf config
if [ -d deployment/config ]; then
    mv deployment/config config
fi
rm -rf deployment
popd >/dev/null

echo
echo 'Build Package'
pushd $BUILD_FOLDER >/dev/null
COPYFILE_DISABLE=1 tar -czvf $PACKAGE .
popd

PASSWORD="$(getval password build 2>/dev/null || true)"
if [ -n "$PASSWORD" ]; then
    echo
    echo "Encrypting build"
    mv $PACKAGE $PACKAGE.tmp
    openssl aes-256-cbc -in $PACKAGE.tmp -out $PACKAGE -pass "pass:$PASSWORD"
fi

echo
if [ -n "$S3_PATH" ]; then
    echo "Uploading to S3"
    if ! aws s3 cp $PACKAGE $S3_PATH --content-type 'application/tar+gzip'; then
        echo 'Uploading failed. Make sure awscli is installed and configured.'
        echo '  1. pip install awscli'
        echo '  2. aws configure'
        exit 2
    fi

    echo
    echo 'Download from this URL (expires in 1 day)'
    signed_url=$(aws s3 presign $S3_PATH_FULL --expires-in 86400)
    echo $signed_url

    echo
    echo "<a href='${signed_url}'>${PACKAGE_FILE}</a>" >/tmp/dl.html
    aws s3 cp /tmp/dl.html $S3_PATH --acl public-read
else
    echo 'Skipping upload to S3. Please configure package.s3_path to enable.'
    echo "Package is in $PACKAGE"
fi

echo
echo 'Done'
