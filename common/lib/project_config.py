"""Helper to parse project configuration.
"""
import argparse
import logging
import os.path
import time
from ConfigParser import SafeConfigParser

from squirro_client import ItemUploader, SquirroClient

log = logging.getLogger(__name__)


def get_config(config_file=None):
    """Parse the config file and return a ConfigParser object.

    Always reads the projects `main.ini` file. If `config_file` is specified,
    that file is added on top.
    """
    root = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../'))
    cfg = SafeConfigParser(defaults={'root': root})

    project_config_file = os.path.join(root, 'config/main.ini')
    files = [project_config_file]
    if config_file:
        files.append(config_file)

    log.debug('Reading config files: %r', files)
    cfg.read(files)
    return cfg


def update_args(config, args, keys=None, section=None):
    """Extends the command line arguments with values read from the config.

    By default adds the Squirro connection parameters. If the `keys` and
    `section` are specified, those are also added (by getting them from the
    given section in the config file).
    """
    all_keys = {
        'squirro': ['token', 'cluster', 'project_id']
    }
    if keys or section:
        assert keys and section, \
            '`keys` and `section` must be specified together'
    if keys and section:
        all_keys.setdefault(section, []).extend(keys)

    for section, keys in all_keys.iteritems():
        for key in keys:
            if hasattr(args, key) and not getattr(args, key):
                if not config.has_option(section, key):
                    raise ValueError(
                        'Missing configuration {0} in section {1}'.format(
                            key, section))
                setattr(args, key, config.get(section, key))

    return args


def get_arg_parser(squirro=True, project_id=True):
    """Returns a standard argument parser to use for most command line scripts.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', action='count',
                        help='Show additional information.')
    parser.add_argument('--log-file', dest='log_file',
                        help='Log file on disk.')
    parser.add_argument('--config-file', dest='config_file',
                        help='Configuration file to read settings from.')
    if squirro:
        parser.add_argument('--token', dest='token',
                            help='Squirro API Token')
        parser.add_argument('--cluster', dest='cluster',
                            help='Squirro API URL')
        if project_id:
            parser.add_argument('--project-id', dest='project_id',
                                help='Source project ID'),
    return parser


def setup_logging(args):
    """Set up logging based on the command line options.
    """
    # Set up logging
    fmt = '%(asctime)s %(name)s %(levelname)-8s %(message)s'
    if args.verbose == 1:
        level = logging.INFO
        logging.getLogger(
            'requests.packages.urllib3.connectionpool').setLevel(logging.WARN)
    elif args.verbose >= 2:
        level = logging.DEBUG
    else:
        # default value
        level = logging.WARN
        logging.getLogger(
            'requests.packages.urllib3.connectionpool').setLevel(logging.WARN)

    # configure the logging system
    if args.log_file:
        out_dir = os.path.dirname(args.log_file)
        if out_dir and not os.path.exists(out_dir):
            os.makedirs(out_dir)
        logging.basicConfig(
            filename=args.log_file, filemode='a', level=level, format=fmt)
    else:
        logging.basicConfig(level=level, format=fmt)

    # Log time in UTC
    logging.Formatter.converter = time.gmtime


def get_client(config, section='squirro'):
    """
    Instantiate a Squirro client based on the config file.

    `section` can be changed to allow multiple connections in one config file.
    """
    cluster = config.get(section, 'cluster')
    token = config.get(section, 'token')
    client = SquirroClient(None, None, cluster=cluster)
    client.authenticate(refresh_token=token)
    return client


def get_uploader(config, source_name=None, section='squirro'):
    """
    Instantiate a Squirro ItemUploader based on the config file.

    `source_name` specifies the name of the Squirro source. If this isn't
    passed in, it's specified in the config file and if that can't be found, it
    falls back to the default value.

    `section` can be changed to allow multiple connections in one config file.
    """
    cluster = config.get(section, 'cluster')
    token = config.get(section, 'token')
    project_id = config.get('squirro', 'project_id')
    if not source_name and config.has_option(section, 'source_name'):
        source_name = config.get(section, 'source_name')

    uploader = ItemUploader(
        cluster=cluster,
        token=token,
        project_id=project_id,
        source_name=source_name,
    )
    return uploader
