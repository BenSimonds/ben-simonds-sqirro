"""
This pipelet uses the spacy library to extract entities
"""

from squirro.sdk import PipeletV1, require
import spacy
from bs4 import BeautifulSoup


@require('log')
class SpacyPipelet(PipeletV1):


    def __init__(self, config):

        # Only load spacy once per batch
        self.nlp = spacy.load('en')


    def consume(self, item):

        nlp = self.nlp
        self.log.warn('Loaded Spacy')

        body_text = self.get_body_text(item)
        spacy_doc = self.create_spacy_doc(body_text, nlp)

        self.process_sentences(item, spacy_doc, nlp)
        self.log.warn('Finished Pipelet')

        return item


    def get_body_text(self, item):

        if '<' in item['body']:

            soup = BeautifulSoup(item['body'], 'html.parser')
            visible_text = soup.getText()
            return visible_text

        else:
            return item['body']


    def create_spacy_doc(self, text, nlp):
        # parse the HTML body in the item if necessary, return a spacy doc

        return nlp(unicode(text))


    def process_sentences(self, item, spacy_doc, nlp):
        # Process each sentence in the doc separately

        for sentence in spacy_doc.sents:

            self.add_keyword(item, 'roots', sentence.root)

            sentence_doc = self.create_spacy_doc(sentence, nlp)

            self.tag_entities(item, sentence_doc, sentence)
            #self.tag_subjects(item, sentence)

        return item


    def tag_entities(self, item, sentence_doc, sentence):
        # Tags entities in a sentence

        for entity in sentence_doc.ents:

            self.add_keyword(item, 'entities', entity)
            #self.add_keyword(item, 'ent_' + str(entity.label_), entity)

            if entity.label_ in ['GPE', 'LOC']:
                self.add_keyword(item, 'Locations', entity)

            if entity.label_ in ['ORG']:
                self.add_keyword(item, 'Organizations', entity)

            if entity.label_ in ['PERSON']:
                self.add_keyword(item, 'People', entity)

            if entity.label_ in ['PRODUCT']:
                self.add_keyword(item, 'Products', entity)

            if entity.label_ in ['DATE']:
                self.add_keyword(item, 'Dates', entity)

        for noun_chunk in sentence_doc.noun_chunks:

            self.add_keyword(item, 'noun_chunks', noun_chunk)
            #self.add_keyword(item, 'entities_' + str(sentence.root.lemma_), noun_chunk)
            if (
                str(sentence.root.lemma_) not in ['say', 'be', 'tell', 'appear', 'add', 'think', "'"]
            ) and (
                str(noun_chunk).lower() not in ['it']
            ):
                self.add_keyword(item, 'noun_verb_pair', self.get_clean_value(str(noun_chunk)) + ' ' + str(sentence.root.lemma_) + 's')

        return item


    def tag_subjects(self, item, sentence):
        # Identify Verb-Subject pairs

        sentence_doc = self.nlp(unicode(sentence))

        sentence_noun_chunks = sentence_doc.noun_chunks

        for chunk in sentence_noun_chunks:
            for word in chunk:
                if str(word.head.lemma_) == str(sentence.root.lemma_):
                    self.add_keyword(item, 'chunks_' + word.head.lemma_, chunk)

        return item


    def add_keyword(self, item, facet_name, value):

        value = self.get_clean_value(str(value))

        # Safely add a value to a given facet
        facet_name = str(facet_name)
        value = str(value)

        item.setdefault('keywords', {})
        item['keywords'].setdefault(facet_name, [])

        if value in item['keywords'][facet_name]:
            return item

        else:
            item['keywords'][facet_name].append(value)
            return item


    def get_clean_value(self, value):

        words_to_eliminate = ['a', 'the', 'its']

        value = ' ' + value + ' '

        for word in words_to_eliminate:
            word_string = ' ' + word + ' '
            if word_string in value:
                value = value.replace(word_string, ' ')

        value = value.strip()
        return value.lower()
