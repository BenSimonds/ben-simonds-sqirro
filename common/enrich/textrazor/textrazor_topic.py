"""
This pipelet enriches each incoming item with concept data from TextRazor.
"""

from squirro.sdk import PipeletV1, require
from hashlib import sha256
import json
import os
import errno

API_URL = 'https://api.textrazor.com'

@require('requests')
@require('log')
class TextRazorTopicPipelet(PipeletV1):

    def __init__(self, config):
        if not 'api_key' in config:
            raise ValueError('Missing API key')

        if not 'coarse_topics' in config:
            config['coarse_topics'] = True

        if not 'coarse_topics_limit' in config:
            config['coarse_topics_limit'] = 0.75

        if not 'topics' in config:
            config['topics'] = True

        if not 'topics_limit' in config:
            config['topics_limit'] = 0.75

        if not 'coarse_topic_target_keyword' in config:
            config['coarse_topic_target_keyword'] = 'Topics (High Level)'

        if not 'topic_target_keyword' in config:
            config['topic_target_keyword'] = 'Topics'


        self.config = config

    def consume(self, item):
        self._enrich(item)
        return item

    def _enrich(self, item):

        keywords = item.setdefault('keywords', {})
        keywords['textrazor_topic'] = ["1"]

        if not 'body' in item:
            return

        data = {
            'apiKey': self.config['api_key'],
            'text': item['body'],
            'cleanup.mode': 'cleanHTML',
            'extractors': 'topics'
        }
        headers = {
            'Accept': 'application/json',
        }

        #try cache
        res_data = self._lookup_cache(data['text'])

        if not res_data:
            #cache miss
            res = self.requests.post(API_URL, headers=headers, data=data)

            if res.status_code > 200:
                self.log.warn('Got invalid status code from TextRazor: %r', res)
                return

            try:
                res_data = res.json()
                self._write_cache(data['text'], res_data) #cache

            except ValueError:
                self.log.exception('Error parsing AlchemyAPI response')
                return

        if self.config['coarse_topics']:
            coarse_topics = res_data['response'].get('coarseTopics', [])

            filtered_coarse_topics = []

            for coarse_topic in coarse_topics:

                if coarse_topic['score'] < self.config['coarse_topics_limit']:
                    continue

                filtered_coarse_topics.append(coarse_topic['label'])

            if len(filtered_coarse_topics) > 0:
                keywords[self.config['coarse_topic_target_keyword']] = filtered_coarse_topics

        if self.config['topics']:
            topics = res_data['response'].get('topics', [])

            filtered_topics = []

            for topic in topics:

                if topic['score'] < self.config['topics_limit']:
                    continue

                filtered_topics.append(topic['label'])

            if len(filtered_topics) > 0:
                keywords[self.config['topic_target_keyword']] = filtered_topics

    def _lookup_cache(self, body, ):

        file_path = self._cache_file_name(body)
        path = os.path.dirname(file_path)
        try:
            os.makedirs(path)
        except OSError as ex:
            if ex.errno != errno.EEXIST:
                return None
            else:
                self.log.exception('Could not create cache folder %s', path)
                return None

        try:
            with open(file_path, 'rb') as f:
                return json.load(f)
        except Exception:
            return None

    def _write_cache(self, body, keywords):
        file_path = self._cache_file_name(body)
        try:
            with open(file_path, 'wb') as f:
                json.dump(keywords, f)
        except Exception as ex:
            self.log.exception('Could not write cache file')

    def _cache_file_name(self, body):

        digest_body = repr(body)
        digest = sha256(digest_body).hexdigest()
        path = os.path.join('/tmp/textrazor/topic/', digest[:2], digest[2:4], digest[4:6])
        file_path = path + '/' + digest + '.json'
        return file_path
