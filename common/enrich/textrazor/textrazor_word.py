"""
This pipelet enriches each incoming item with word data from TextRazor.
"""

from squirro.sdk import PipeletV1, require
from hashlib import sha256
import json
import os
import errno

API_URL = 'https://api.textrazor.com'

@require('requests')
@require('log')
class TextRazorTopicPipelet(PipeletV1):

    def __init__(self, config):
        self.config = config

    def consume(self, item):
        self._enrich(item)
        return item


    def _enrich(self, item):

        keywords = item.setdefault('keywords', {})
        keywords['textrazor_word'] = ["1"]

        if 'body' in item:
            body = item['body']
        elif 'sub_items' in item:
            body = ''

            for sub_item in item.get('sub_items'):

                if len(body) > 180 * 1024:
                    break

                body += sub_item.get('body', '')

            if body == '':
               return
        else:
            return

        data = {
            'apiKey': self.config['api_key'],
            'text': body,
            'cleanup.mode': 'cleanHTML',
            'extractors': 'words'
        }
        headers = {
            'Accept': 'application/json',
        }

        #try cache
        res_data = self._lookup_cache(data['text'])

        if not res_data:
            #cache miss
            res = self.requests.post(API_URL, headers=headers, data=data)

            if res.status_code > 200:
                self.log.warn('Got invalid status code from TextRazor: %r', res)
                return

            try:
                res_data = res.json()
                self._write_cache(data['text'], res_data) #cache

            except ValueError:
                self.log.exception('Error parsing AlchemyAPI response')
                return

        response = res_data.get('response', {})
        sentences = response.get('sentences', [])
        extracted_words = []

        for sentence in sentences:

            for word in sentence['words']:

                if word['partOfSpeech'] == 'NN' or word['partOfSpeech'] == 'NNP' or word['partOfSpeech'] == 'NNS':

                    if len(word['lemma']) > 4:
                        extracted_words.append(word['lemma'])

        #deduplicate
        extracted_words = list(set(extracted_words))
        keywords['Keywords'] = extracted_words

    def _lookup_cache(self, body, ):

        file_path = self._cache_file_name(body)
        path = os.path.dirname(file_path)
        try:
            os.makedirs(path)
        except OSError as ex:
            if ex.errno != errno.EEXIST:
                return None
            else:
                self.log.exception('Could not create cache folder %s', path)
                return None

        try:
            with open(file_path, 'rb') as f:
                return json.load(f)
        except Exception:
            return None

    def _write_cache(self, body, keywords):
        file_path = self._cache_file_name(body)
        try:
            with open(file_path, 'wb') as f:
                json.dump(keywords, f)
        except Exception as ex:
            self.log.exception('Could not write cache file')

    def _cache_file_name(self, body):

        digest_body = repr(body)
        digest = sha256(digest_body).hexdigest()
        path = os.path.join('/tmp/textrazor/words/', digest[:2], digest[2:4], digest[4:6])
        file_path = path + '/' + digest + '.json'
        return file_path
