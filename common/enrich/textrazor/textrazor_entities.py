"""
This pipelet enriches each incoming item with sentiment data form AlchemyAPI.
"""
from squirro.sdk import PipeletV1, require
import os
import os.path
import json
from datetime import datetime
import requests
from hashlib import sha256
import errno

TEXTRAZOR_ENDPOINT = 'http://companyoffice.squirro.net:8301/'
FREEBASE_FILTERS = {
    '/finance/currency': 'Currency',
    '/organization/organization': 'Organization',
    '/location/country': 'Geo_State',
    '/engineering/material': 'Material',
    '/chemistry/chemical_compound': 'Material',
    '/chemistry/chemical_element': 'Material',
    '/business/market_index': 'Market index',
    '/location/citytown': 'Geo_City',
    '/location/region': 'Geo_Region',
    '/organization/organization_sector': 'Sector'
}


@require('requests')
@require('log')
class TextrazorPipelet(PipeletV1):
    def __init__(self, config):
        if not 'api_key' in config:
            raise ValueError('Missing API key')

        # Only entities extractor works so far
        config['extractors'] = ['entities']

        self.config = config
        self.endpoint = self.config.get('endpoint') or TEXTRAZOR_ENDPOINT

    def consume(self, item):
        self._enrich(item)
        return item

    def _enrich(self, item):
        texts = filter(None, [item.get('title'), item.get('body')])
        body = "\n".join(texts)
        if not body.strip():
            return

        # TextRazor fails completely with HTML cleanup set on plaintext
        # body
        cleanup = '<' in body

        data = {
            'text': body,
            'apiKey': self.config['api_key'],
            'extractors': self.config.get('extractors', []),
            'cleanupHTML': cleanup,
            'entities.filterFreebaseTypes': FREEBASE_FILTERS.keys(),
        }

        res = self.get_response(
                request=self.endpoint,
                cache_location='/tmp/response_cache/',
                timeout=None,
                http='POST',
                data=data
                )

        res = res['data']

        entities = res.get('response', {}).get('entities', [])
        if not 'keywords' in item:
            item['keywords'] = {}
        self._transform_response(entities, item)

    def _transform_response(self, entities, item):
        entities.sort(key=lambda x: x['relevanceScore'], reverse=True)
        transform = self.config.get('transform', {})

        seen = set()
        for entity in entities:
            entity_id = entity.get('entityEnglishId') or entity['entityId']
            if entity_id not in seen:
                seen.add(entity_id)

                for hit in entity['freebaseTypes']:
                    keyword_name = FREEBASE_FILTERS.get(hit)
                    if keyword_name:
                        value = entity_id
                        replace_value = transform.get(keyword_name, {}).get(value)
                        if replace_value:
                            value = replace_value

                        if value == 'skip':
                            continue  # we skip this value

                        item['keywords'].setdefault(keyword_name, []).append(value)



    # THE API RESPONSE CACHING FRAMEWORK
    # The main method, This is what is called to get a remote cached API response
    def get_response(self, request, cache_location='/tmp/response_cache/', store_errors=False, timeout=None, http='GET', headers=None, data=None):

        # Check the cache first, then if no cache, go to the API to get the folder data
        response_data = self._lookup_cache(request, headers, data, cache_location, timeout)

        # Cache Miss
        if not response_data:
            # Make API Request
            response = self.get_web_response(request, http, headers, data)

            if (store_errors) or (int(response['status_code']) < 300):

                try:
                    # Write to cache after successful API response
                    self._write_cache(request, headers, data, response, cache_location)

                except ValueError:

                    # If writing the first time fails, it is useful to try
                    # a second time with the encoding set
                    try:
                        self._write_cache(request, headers, data, response.encode('utf8'), cache_location)

                    except UnicodeEncodeError:
                        print 'Error writing the response to the cache'

        # Cache Hit
        else:
            # work with the res_data as if it were the API response
            response = response_data

        return response


    def get_web_response(self, request, http, headers, data):

        if http in ['GET', 'Get', 'get']:
            response = requests.get(request, headers=headers, data=data)

        elif http in ['POST', 'Post', 'post']:
            response =  requests.post(request, headers=headers, data=data)

        else:
            return None


        try:
            response_data = json.loads(response.text)

        except Exception:
            response_data = response.text

        response_status = response.status_code

        response_dict = {
            "data": response_data,
            "status_code": response_status
        }

        return response_dict


    def _lookup_cache(self, request, headers, data, cache_location, timeout):

        file_path = self._cache_file_name(request, headers, data, cache_location)
        path = os.path.dirname(file_path)

        try:
            os.makedirs(path)

        except OSError as ex:
            if ex.errno != errno.EEXIST:
                print 'Could not create cache folder %s', path
                return None

        try:

            # Try to get the last modified date of the file
            if (self.get_cache_age(file_path) < timeout) or (not timeout):
                with open(file_path, 'rb') as f:

                    response = json.load(f)['response']
                    return response

            else:
                return None

        except Exception:
            return None


    def get_cache_age(self, file_path):

        modified_time = self.get_modified_time(file_path)
        current_time = datetime.now()

        cache_age = current_time - modified_time
        age_days = cache_age.days
        age_seconds = cache_age.seconds
        # Calculate the total number of second elapsed
        total_age_seconds = age_seconds + (86400 * age_days)

        # Convert the total age in seconds into hours (rounded down)
        total_age_hours = total_age_seconds // 3600

        return total_age_hours



    def get_modified_time(self, file_path):

        time_stamp = os.path.getmtime(file_path)

        return datetime.fromtimestamp(time_stamp)


    def _write_cache(self, request, headers, data, response, cache_location):

        file_path = self._cache_file_name(request, headers, data, cache_location)
        json_object = {'cache-key': request, 'response': response}

        with open(file_path, 'wb') as f:
            json.dump(json_object, f)


    def _cache_file_name(self, request, headers, data, cache_location):

        digest_body = str({
            "request": repr(request),
            "headers": headers,
            "data": data
            })
        digest = sha256(digest_body).hexdigest()

        path = os.path.join(cache_location, digest[:2], digest[2:4], digest[4:6])
        file_path = path + '/' + digest + '.json'

        return file_path
