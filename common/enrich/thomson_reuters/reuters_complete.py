"""
This pipelet enriches each incoming items with Open Calais and TR Open PermID.
"""

from squirro.sdk import PipeletV1, require
from hashlib import sha256
import json
import os
import time
import os.path
import errno
import csv
import requests
from datetime import datetime
import errno
from jinja2 import Template

CALAIS_API_URL = "https://api.thomsonreuters.com/permid/calais"

@require('requests')
@require('log')
class OpenCalaisPipelet(PipeletV1):

    def __init__(self, config):
        self.config = config

        if not config.get('confidence'):
            self.config['confidence'] = 0.75


    def consume(self, item):

        # Early exit for if the item is totally useless to tagging
        useless_item = self.check_for_useless_doc(item)

        if useless_item:
            self.log.info('Found a useless item. Aborting')
            return False

        self._enrich(item)
        self.create_derived_facets(item)
        self.remove_reuters_tags(item)
        self.add_tags_to_body(item)
        self.double_space_body(item)
        item['keywords']['opencalais'] = ['13']
        item['keywords']['tr_processing'] = ['7']

        return item


    def check_for_useless_doc(self, item):
        # A bunch of manual checks for things that we don't want to tag

        if 'charts.tradingcentral.com' in item['body']:
            return True

        elif 'Click the following link to watch video' in item['body']:
            return True

        elif '*TOP NEWS*' in item['title']:
            return True

        return False


    def double_space_body(self, item):
        # Make reading the item body easier by changing all the newlines to double newlines

        item['body'] = item['body'].replace('\n', '<br />')
        return item


    def add_tags_to_body(self, item):

        jinja_template = '''{% if item['keywords'].get('Company_PermID_Link') %}<u>Companies referenced:</u> <br />{% for i in range(0, item['keywords']['Company_PermID_Link']|length) %}<a href="{{item['keywords']['Company_PermID_Link'][i]}}"><b>{{item['keywords']['Company'][i]}}</b></a><br />{% endfor %}<br />{% endif %}{% if item['keywords'].get('topics_rcs') %}<u>Topics Referenced:</u> <br />{% for i in range(0, item['keywords']['topics_rcs']|length) %}<b>{{item['keywords']['topics_rcs'][i]}}</b> ({{item['keywords']['topics'][i]}}) - {{item['keywords']['topics_description'][i]}}<br />{% endfor %}{% endif %} <br /> <br />'''

        template = Template(jinja_template)
        tags_section = template.render(item=item)

        item['body'] = tags_section + item['body']

        return item


    def get_derived_event_type(self, event_type):

        groups = {
            "Earnings": ['AnalystEarningEstimate', 'CompanyEarningAnnouncement', 'CompanyEarningGuidance', 'Dividend', 'ConferenceCall', 'CompanyMeeting'],
            "New_Issues": ['SecondaryIssuance', 'IPO', 'EquityFinancing', 'BonusSharesIssuance'],
            "Drew_Down_on_Revolver": ['DebtFinancing', 'BuyBack', 'Bankruptcy', 'EquityFinancing', 'Dividend', 'CompanyInvestment'],
            "M&A": ['Acquisition', 'Merger', 'Deal'],
            "Forecast_Change": ['CompanyRestatement', 'AnalystEarningEstimate', 'CompanyEarningAnnouncement', 'CompanyEarningGuidance', 'AnalystRecommendation', 'CompanyAccountingChange'],
            "Top_Management_Change": ['CompanyReorganization', 'CompanyLayoffs', 'EmploymentChange'],
            "Rating_Agency_Changes": ['CreditRating'],
            "IP_Changes": ['CompanyLegalIssues', 'PatentFiling', 'PatentIssuance'],
            "Regulation": ['CompanyInvestigation', 'FDAPhase']
        }

        for key, value in groups.iteritems():
            if event_type in value:
                return key

        return None


    def create_derived_facets(self, item):

        groups = {
            "Earnings": ['analyst_estimate_company', 'earnings_announcement_company', 'earnings_guidance_company', 'dividend_company', 'conferencecall_company', 'meeting_company'],
            "New_Issues": ['secondary_issuance_company', 'ipo_company', 'equity_financing_company', 'bonus_issuance_company'],
            "Drew_Down_on_Revolver": ['debt_financing_company', 'buyback_company', 'bankruptcy_company', 'equity_financing_company', 'dividend_company', 'investment_company'],
            "M&A": ['acquisition_acquirer', 'acquisition_beingacquired', 'merger_company', 'deal_acquirer', 'deal_target'],
            "Forecast_Change": ['restatement_company', 'analyst_estimate_company', 'earnings_announcement_company', 'earnings_guidance_company', 'analyst_rec_company', 'accounting_change_company'],
            "Top_Management_Change": ['reorganization_company', 'layoff_company', 'employment_change_company'],
            "Rating_Agency_Changes": ['credit_company_rated', 'credit_company_rated'],
            "IP_Changes": ['legal_issues_company_plaintiff', 'legal_issues_company_sued', 'patent_filing_company', 'patent_issuance_company'],
            "Regulation": ['investigation_company']
        }

        formatted_names = {
            "Earnings": "Earnings",
            "New_Issues": "New Issues",
            "Drew_Down_on_Revolver": "Drew Down On Revolver",
            "M&A": "M&A",
            "Forecast_Change": "Forecast Change",
            "Top_Management_Change": "Top Management Change",
            "Rating_Agency_Changes": "Rating Agency Change",
            "IP_Changes": "IP Changes",
            "Regulation": "Regulation"
        }

        for group_name, facet_list in groups.iteritems():

            for facet in facet_list:

                if item['keywords'].get(facet):

                    item['keywords'].setdefault(group_name, [])
                    item['keywords'][group_name] = list(set(item['keywords'][group_name] + item['keywords'][facet]))
                    item['keywords'].setdefault('Derived_Events', [])

                    if len(item['keywords'][group_name]) > 0:
                        self.tag_with_text('Derived_Events', formatted_names[group_name], item)

            if item['keywords'].get(group_name):
                for company_listed in item['keywords'][group_name]:
                    self.tag_with_text('Derived_Event_Company', group_name + '|' + company_listed, item)

        return item


    def remove_reuters_tags(self, item):

        values_to_remove = ['Reuters', 'Thomson Reuters', 'ThomsonReuters', 'Reuters Group', 'https://permid.org/1-4295861160', '4295861160']
        keys_to_remove = []

        for facet, value in item['keywords'].iteritems():
            item['keywords'].setdefault(facet, [])

            for bad_value in values_to_remove:

                if item['keywords'].get(facet) != None:

                    while bad_value in item['keywords'].get(facet):

                        item['keywords'][facet].remove(bad_value)

                        if len(item['keywords'].get(facet)) == 0:
                            if facet not in keys_to_remove:
                                keys_to_remove.append(facet)
                            break

        for key in keys_to_remove:

            if item['keywords'].get(key):

                del item['keywords'][key]

        return item


    def _enrich(self, item):

        keywords = item.setdefault('keywords', {})

        span_tags = []

        if not self.config.get('api_key'):
            self.log.error('No opencalias api key present, please set api_key in the pipelet config.')
            return

        #prep the item
        if 'body' in item:
            body = item['body']

        elif 'sub_items' in item:
            body = ''

            for sub_item in item.get('sub_items'):

                if len(body) > 180 * 1024:
                    break

                body += sub_item.get('body', '')

            if body == '':
               return

        else:
            return

        headers = {
            'Content-Type': 'text/html; charset=utf-8',
            'outputFormat': 'application/json',
            'x-calais-language': 'English',
            'x-ag-access-token': self.config.get('api_key')
            }

        data=body.encode('utf-8')

        # Delay to avoid going over the API Key usage limit
        time.sleep(2)

        res_data = self.get_response(
            request=CALAIS_API_URL,
            cache_location='/tmp/response_cache/',
            timeout=None,
            http='POST',
            headers=headers,
            data=data
            )

        res_data = res_data['data']

        if not isinstance(res_data, dict):
            self.log.error('Got the bad API response: {}'.format(repr(res_data)))
            item['keywords']['bad_response'] = ['True']
            return item

        # Swap out the body for the clean one used by open calais
        item['body'] = res_data['doc']['info']['document']

        # Loop over every item in the API response, and add a tag for it
        for key, value in res_data.iteritems():

            # Skip the key/value pair with metadata about the doc sent
            if (key == 'doc') or (value['_typeGroup'] == 'versions'):
                continue

            elif value['_typeGroup'] in ['topics']:

                self.tag_topic(value, item)

            elif value['_typeGroup'] in ['industry', 'socialTag']:

                self.tag_with_text(value.get('_typeGroup'), value.get('name'), item)

            # Process Entities
            elif value['_typeGroup'] == 'entities':

                if value['_type'] == 'Company':

                    resolution = value.get('resolutions')

                    if resolution:
                        company_name = resolution[0].get('commonname')
                        company_permid = resolution[0].get('permid')
                        company_permid_link = resolution[0].get('id')

                        try:
                            self.tag_with_text('Company_PermID', company_permid, item)
                            self.tag_with_text('Company_PermID_Link', company_permid_link, item)
                            self.tag_with_text('Company', company_name, item)
                            self.tag_relevance(value.get('relevance'), company_name, item)
                        except:
                            self.log.error('Missing a Needed parameter for PermID Resolution: ')
                            self.log.error(resolution[0])

                else:
                    self.tag_with_text(value.get('_type'), value.get('name'), item)

            # Processs Relations
            elif value['_typeGroup'] == 'relations':

                self.process_relation(value, res_data, item, span_tags)

        if len(span_tags) > 0:
            self.add_span_tags_to_body(item, span_tags)

        return item


    def add_span_tags_to_body(self, item, span_tags):

        sorted_tags = sorted(span_tags, key=lambda tag: tag['index'])

        while len(sorted_tags) > 0:
            tag = sorted_tags.pop()

            tag_index = int(tag['index'])

            # Add the closing span tag, then the opening span tag
            item['body'] = item['body'][:tag_index] + tag['tag'] + item['body'][tag_index:]

        return item


    def tag_relevance(self, relevance_score, company_name, item):

        if not relevance_score:
            return item

        else:
            facet_name = 'relevance_' + str(float(relevance_score))
            item['keywords'].setdefault(facet_name, [])

            if company_name not in item['keywords'][facet_name]:
                item['keywords'][facet_name].append(company_name)

            return item


    def tag_topic(self, value, item):

        if value.get('rcscode'):

            rcs_description = self.lookup_rcs(value.get('rcscode'))
            if rcs_description:
                self.tag_with_text('topics_description', rcs_description, item)
                self.tag_with_text('topics_rcs', value.get('rcscode'), item)

                if value.get('name'):
                    self.tag_with_text('topics', value.get('name'), item)


    def lookup_rcs(self, code):

        rcs_code_file = self.get_response(
            request='https://gist.githubusercontent.com/alexdelin/d7c16d9be402a5bdec2416a0d60d1de0/raw/93f9ad80988a71c5c5bf2546dee7f467d28d9b14/rsc.csv',
            cache_location='/tmp/response_cache/',
            timeout=None,
            http='GET'
            )['data']

        rcs_code_data = csv.DictReader(rcs_code_file.splitlines())

        for row in rcs_code_data:
            if row['rcs'] == code:
                return row['description']

        return 'No Description Found'


    def get_involved_companies(self, value, res_data):

        # Have to use the raw OpenCalais field names, not the facet names
        fields_to_check = ['company', 'target', 'acquirer', 'company_beingacquired', 'company_acquirer', 'company_source', 'company_rated', 'organization_rated', 'company_plaintiff', 'company_sued', 'company_investor', 'company_investigated', 'company_affiliate', 'company_parent']

        involved_companies = []

        for field in fields_to_check:

            if value.get(field):

                if isinstance(value.get(field), list):

                    for company_hash in value.get(field):

                        company_name = self.get_entity_name(company_hash, res_data)
                        involved_companies.append(company_name)

                else:
                    # need to lookup the hash value and turn it into a company name
                    company_name = self.get_entity_name(value.get(field), res_data)
                    involved_companies.append(company_name)

        return involved_companies


    def process_relation(self, value, res_data, item, span_tags):


        #if value['_type'] in ['AnalystEarningEstimate', 'CompanyEarningAnnouncement', 'CompanyEarningGuidance', 'ConferenceCall', 'CompanyMeeting', 'Dividend', 'BonusSharesIssuance', 'BuyBack', 'Deal', 'DebtFinancing', 'EquityFinancing', 'IPO', 'SecondaryIssuance','Merger', 'Acquisition', 'Alliance', 'CompanyAffiliate', 'Deal', 'JointVenture','CompanyAccountingChange', 'CompanyRestatement', 'DelayedFilings', 'StockSplit', 'EmploymentChange','AnalystEarningEstimate', 'CreditRating', 'AnalystRecommendation', 'FDAPhase', 'PatentIssuance', 'PatentFiling']:
        if value.get('instances'):
            event = value.get('_type')
            derived_event_type = self.get_derived_event_type(event)
            detection_string = value['instances'][0]['exact']
            length = value['instances'][0]['length']
            company_involved = self.get_involved_companies(value, res_data)
            #self.log.error(company_involved)
            self.add_to_span_tags(event, derived_event_type, detection_string, length, item, span_tags, company_involved)

        if value['_type'] in ['Acquisition', 'EmploymentChange', 'AnalystEarningsEstimate', 'CompanyEarningsAnnouncement', 'CompanyEarningsGuidance', 'AnalystRecommendation', 'SecondaryIssuance', 'BonusSharesIssuance', 'EquityFinancing', 'DebtFinancing', 'Deal', 'CompanyRestatement', 'CompanyReorganization', 'CreditRating', 'PatentFiling', 'PatentIssuance', 'IPO', 'CompanyLegalIssues', 'Dividend', 'Buybacks', 'Merger', 'Bankruptcy', 'CompanyAccountingChange', 'CompanyInvestigation', 'CompanyInvestment', 'CompanyLayoffs']:
            self.tag_with_text('Relation_Event', value['_type'], item)

        if value['_type'] == 'PersonCareer':

            self.tag_with_hash('personcareer_company', value.get('company'), res_data, item)
            self.tag_with_hash('personcareer_person', value.get('person'), res_data, item)
            self.tag_with_text('personcareer_position', value.get('position'), item)

        elif value['_type'] == 'ConferenceCall':

            self.tag_with_hash('conferencecall_company', value.get('company'), res_data, item)
            self.tag_with_text('conferencecall_type', value.get('conferencecalltype'), item)

        elif value['_type'] == 'CompanyMeeting':

            self.tag_with_hash('meeting_company', value.get('company'), res_data, item)
            self.tag_with_text('meeting_type', value.get('companymeetingtype'), item)

        elif value['_type'] == 'Quotation':

            pass
            #self.process_quote(value, res_data, item)

        elif value['_type'] == 'Acquisition':

            self.tag_with_hash('acquisition_acquirer', value.get('company_acquirer'), res_data, item)
            self.tag_with_hash('acquisition_beingacquired', value.get('company_beingacquired'), res_data, item)

        elif value['_type'] == 'CompanyAffiliates':

            self.tag_with_hash('affiliate_parent', value.get('company_parent'), res_data, item)
            self.tag_with_hash('affiliate_affiliate', value.get('company_affiliate'), res_data, item)

        elif value['_type'] == 'Buybacks':

            self.tag_with_hash('buyback_company', value.get('company'), res_data, item)

        elif value['_type'] == 'Merger':

            self.tag_with_hash('merger_company', value.get('company'), res_data, item)

        elif value['_type'] == 'Bankruptcy':

            self.tag_with_hash('bankruptcy_company', value.get('company'), res_data, item)

        elif value['_type'] == 'CompanyAccountingChange':

            self.tag_with_hash('accounting_change_company', value.get('company'), res_data, item)
            self.tag_with_text('accounting_change_type', value.get('accountingchangetype'), item)

        elif value['_type'] == 'CompanyInvestigation':

            self.tag_with_hash('investigation_company', value.get('company_investigated'), res_data, item)
            self.tag_with_text('investigation_type', value.get('investigationtype'), item)
            self.tag_with_hash('investigation_regulator', value.get('organization_regulator'), res_data, item)
            self.tag_with_hash('investigation_person', value.get('person_investigated'), res_data, item)

        elif value['_type'] == 'CompanyInvestment':

            self.tag_with_hash('investment_company', value.get('company'), res_data, item)
            self.tag_with_hash('investment_investor', value.get('company_investor'), res_data, item)

        elif value['_type'] == 'CompanyLayoffs':

            self.tag_with_hash('layoff_company', value.get('company'), res_data, item)

        elif value['_type'] == 'IPO':

            self.tag_with_hash('ipo_company', value.get('company'), res_data, item)

        elif value['_type'] == 'CompanyLegalIssues':

            self.tag_with_hash('legal_issues_company_sued', value.get('company_sued'), res_data, item)
            self.tag_with_hash('legal_issues_company_plaintiff', value.get('company_plaintiff'), res_data, item)
            self.tag_with_hash('legal_issues_person_plaintiff', value.get('person_plaintiff'), res_data, item)
            self.tag_with_text('legal_issues_lawsuitclass', value.get('lawsuitclass'), item)

        elif value['_type'] == 'Dividend':

            self.tag_with_hash('dividend_company', value.get('company'), res_data, item)
            self.tag_with_text('dividend_type', value.get('dividendtype'), item)

        elif value['_type'] == 'EmploymentChange':

            self.tag_with_hash('employment_change_company', value.get('company'), res_data, item)
            self.tag_with_hash('employment_change_person', value.get('person'), res_data, item)
            self.tag_with_hash('employment_change_organization', value.get('organization'), res_data, item)
            self.tag_with_text('employment_change_position', value.get('position'), item)
            self.tag_with_text('employment_change_type', value.get('changetype'), item)

        elif value['_type'] == 'AnalystEarningsEstimate':

            self.tag_with_hash('analyst_estimate_company', value.get('company_rated'), res_data, item)
            self.tag_with_hash('analyst_estimate_company_source', value.get('company_source'), res_data, item)
            self.tag_with_hash('analyst_estimate_person_source', value.get('person_source'), res_data, item)

        elif value['_type'] == 'CompanyEarningsAnnouncement':

            self.tag_with_hash('earnings_announcement_company', value.get('company'), res_data, item)
            self.tag_with_text('earnings_announcement_metric', value.get('financialmetric'), item)

        elif value['_type'] == 'CompanyEarningsGuidance':

            self.tag_with_hash('earnings_guidance_company', value.get('company'), res_data, item)
            self.tag_with_text('earnings_guidance_metric', value.get('financialmetric'), item)
            self.tag_with_text('earnings_guidance_trend', value.get('financialtrend'), item)

        elif value['_type'] == 'AnalystRecommendation':

            self.tag_with_hash('analyst_rec_company', value.get('company_rated'), res_data, item)
            self.tag_with_hash('analyst_rec_company_source', value.get('company_source'), res_data, item)
            self.tag_with_hash('analyst_rec_person_source', value.get('person_source'), res_data, item)
            self.tag_with_text('analyst_rec_trend', value.get('financialtrend'), item)

        elif value['_type'] == 'SecondaryIssuance':

            self.tag_with_hash('secondary_issuance_company', value.get('company'), res_data, item)

        elif value['_type'] == 'CompanyTicker':

            self.tag_with_hash('ticker_company', value.get('company'), res_data, item)
            self.tag_with_text('ticker_exchange', value.get('stockexchange'), item)
            self.tag_with_text('ticker_ticker', value.get('ticker'), item)

        elif value['_type'] == 'BonusSharesIssuance':

            self.tag_with_hash('bonus_issuance_company', value.get('company'), res_data, item)

        elif value['_type'] == 'EquityFinancing':

            self.tag_with_hash('equity_financing_company', value.get('company'), res_data, item)
            self.tag_with_text('equity_financing_currency', value.get('currency'), item)

        elif value['_type'] == 'DebtFinancing':

            self.tag_with_hash('debt_financing_company', value.get('company'), res_data, item)
            self.tag_with_text('debt_financing_type', value.get('debttype'), item)
            self.tag_with_text('debt_financing_action', value.get('debtaction'), item)

        elif value['_type'] == 'Deal':

            self.tag_with_hash('deal_target', value.get('target'), res_data, item)
            self.tag_with_hash('deal_acquirer', value.get('acquirer'), res_data, item)

        elif value['_type'] == 'CompanyRestatement':

            self.tag_with_hash('restatement_company', value.get('company'), res_data, item)

        elif value['_type'] == 'CompanyReorganization':

            self.tag_with_hash('reorganization_company', value.get('company'), res_data, item)

        elif value['_type'] == 'CreditRating':

            self.tag_with_hash('credit_company_rated', value.get('company_rated'), res_data, item)
            self.tag_with_hash('credit_company_source', value.get('company_source'), res_data, item)
            self.tag_with_hash('credit_country_rated', value.get('country_rated'), res_data, item)
            self.tag_with_hash('credit_organization_rated', value.get('organization_rated'), res_data, item)
            self.tag_with_text('credit_trend', value.get('financialtrend'), item)

        elif value['_type'] == 'PatentFiling':

            self.tag_with_hash('patent_filing_company', value.get('company'), res_data, item)
            self.tag_with_hash('patent_filing_organization', value.get('organization'), res_data, item)
            self.tag_with_text('patent_filing_description', value.get('patentdescription'), item)

        elif value['_type'] == 'PatentIssuance':

            self.tag_with_hash('patent_issuance_company', value.get('company'), res_data, item)
            self.tag_with_hash('patent_issuance_organization', value.get('organization'), res_data, item)
            self.tag_with_text('patent_issuance_description', value.get('patentdescription'), item)

        # Handling for common but untagged responses
        else:
            item['keywords'].setdefault('new_responses', [])
            item['keywords']['new_responses'].append(value['_type'])
            item['keywords']['new_responses'] = list(set(item['keywords']['new_responses']))


    def add_to_span_tags(self, event, derived_event_type, detection_string, length, item, span_tags, companies_involved):

        # Get the index of the beginning of the string within the body
        detection_index = item['body'].find(detection_string)

        # Early exit if the detection string is not found
        if detection_index == (-1):
            item['keywords'].setdefault('tag_errors', [0])
            item['keywords']['tag_errors'] = [item['keywords']['tag_errors'][0] + 1]
            self.log.error('Failed to find string' + detection_string)
            self.log.error('For Event' + event)
            return item

        detection_end_index = detection_index + length

        if derived_event_type:
            event_string = event + '+' + derived_event_type + '|' + '+'.join(companies_involved)
        else:
            event_string = event + '|' + '+'.join(companies_involved)

        # Add the closing span tag, then the opening span tag to the list
        span_tags.append({"tag": "</span>", "index": detection_end_index})
        span_tags.append({"tag": '<span class="event ' + event_string + '">', "index": (detection_index + .5)})

        return item


    def tag_with_text(self, facet_name, text, item):

        if isinstance(text, list):

            for individual_text in text:
                self.tag_with_text(facet_name, individual_text, item)

        else:
            item['keywords'].setdefault(facet_name, [])

            if (text) and (text not in item['keywords'][facet_name]) :
                item['keywords'][facet_name].append(text)


    def tag_with_hash(self, facet_name, hash_value, res_data, item):

        if isinstance(hash_value, list):

            for individual_hash in hash_value:
                self.tag_with_hash(facet_name, individual_hash, res_data, item)

        else:
            item['keywords'].setdefault(facet_name, [])
            temp_value = self.get_entity_name(hash_value, res_data)

            if temp_value:
                item['keywords'][facet_name].append(temp_value)
                item['keywords'][facet_name] = list(set(item['keywords'][facet_name]))


    def get_entity_name(self, entity_hash, res_data):

        if entity_hash == None:
            return None

        else:
            try:

                if res_data[entity_hash].get('resolutions'):
                    return res_data[entity_hash]['resolutions'][0].get('commonname')

                else:
                    return res_data[entity_hash]['name']

            except KeyError:
                return None


    def process_quote(self, value, res_data, item):

        quote_content = value.get('quotation')
        quotation_speaker = self.get_entity_name(value.get('speaker'), res_data)

        if not quote_content:
            return item

        item['keywords'].setdefault('quotation_speaker', [])

        if quotation_speaker:
            item['keywords']['quotation_speaker'].append(quotation_speaker)

        headers = {
            'Content-Type': 'text/html; charset=utf-8',
            'outputFormat': 'application/json',
            'x-calais-language': 'English',
            'x-ag-access-token': self.config.get('api_key')
            }

        data=quote_content.encode('utf-8')

        # Sleep for 2 seconds to avoid going over API Key usage limit
        time.sleep(1)

        res_data = self.get_response(
            request=CALAIS_API_URL,
            cache_location='/tmp/response_cache/',
            timeout=None,
            http='POST',
            headers=headers,
            data=data
            )

        res_data = res_data['data']

        if not isinstance(res_data, dict):
            self.log.error('Got the bad API response: {}'.format(repr(res_data)))
            item['keywords']['bad_quote_response'] = ['True']
            return item

        # Loop over every item in the API response, and add a tag for it
        for key, value in res_data.iteritems():

            # Skip the key/value pair with metadata about the doc sent
            if (key == 'doc') or (value['_typeGroup'] == 'versions'):
                continue

            elif value['_typeGroup'] in ['topics', 'industry', 'socialTag']:

                self.tag_with_text('quote_' + value.get('_typeGroup'), value.get('name'), item)

            # Process Entities
            elif value['_typeGroup'] == 'entities':

                self.tag_with_text('quote_entity', value.get('name'), item)

            elif value['_typeGroup'] == 'relations':

                self.tag_with_text('quote_relation', value.get('_type'), item)

        return item


    # API Response caching framework
    # The main method, This is what is called to get a remote cached API response
    def get_response(self, request, cache_location='/tmp/response_cache/', store_errors=False, timeout=None, http='GET', headers=None, data=None):

        # Check the cache first, then if no cache, go to the API to get the folder data
        response_data = self._lookup_cache(request, headers, data, cache_location, timeout)

        # Cache Miss
        if not response_data:
            # Make API Request
            response = self.get_web_response(request, http, headers, data)

            if (store_errors) or (int(response['status_code']) < 300):

                try:
                    # Write to cache after successful API response
                    self._write_cache(request, headers, data, response, cache_location)

                except ValueError:

                    # If writing the first time fails, it is useful to try
                    # a second time with the encoding set
                    try:
                        self._write_cache(request, headers, data, response.encode('utf8'), cache_location)

                    except UnicodeEncodeError:
                        print 'Error writing the response to the cache'

        # Cache Hit
        else:
            # work with the res_data as if it were the API response
            response = response_data

        return response


    def get_web_response(self, request, http, headers, data):

        if http in ['GET', 'Get', 'get']:
            response = requests.get(request, headers=headers, data=data)

        elif http in ['POST', 'Post', 'post']:
            response =  requests.post(request, headers=headers, data=data)

        else:
            return None


        try:
            response_data = json.loads(response.text)

        except Exception:
            response_data = response.text

        response_status = response.status_code

        response_dict = {
            "data": response_data,
            "status_code": response_status
        }

        return response_dict


    def _lookup_cache(self, request, headers, data, cache_location, timeout):

        file_path = self._cache_file_name(request, headers, data, cache_location)
        path = os.path.dirname(file_path)

        try:
            os.makedirs(path)

        except OSError as ex:
            if ex.errno != errno.EEXIST:
                print 'Could not create cache folder %s', path
                return None

        try:

            # Try to get the last modified date of the file
            if (self.get_cache_age(file_path) < timeout) or (not timeout):
                with open(file_path, 'rb') as f:

                    response = json.load(f)['response']
                    return response

            else:
                return None

        except Exception:
            return None


    def get_cache_age(self, file_path):

        modified_time = self.get_modified_time(file_path)
        current_time = datetime.now()

        cache_age = current_time - modified_time
        age_days = cache_age.days
        age_seconds = cache_age.seconds
        # Calculate the total number of second elapsed
        total_age_seconds = age_seconds + (86400 * age_days)

        # Convert the total age in seconds into hours (rounded down)
        total_age_hours = total_age_seconds // 3600

        return total_age_hours



    def get_modified_time(self, file_path):

        time_stamp = os.path.getmtime(file_path)

        return datetime.fromtimestamp(time_stamp)


    def _write_cache(self, request, headers, data, response, cache_location):

        file_path = self._cache_file_name(request, headers, data, cache_location)
        json_object = {'cache-key': request, 'response': response}

        with open(file_path, 'wb') as f:
            json.dump(json_object, f)


    def _cache_file_name(self, request, headers, data, cache_location):

        digest_body = str({
            "request": repr(request),
            "headers": headers,
            "data": data
            })
        digest = sha256(digest_body).hexdigest()

        path = os.path.join(cache_location, digest[:2], digest[2:4], digest[4:6])
        file_path = path + '/' + digest + '.json'

        return file_path

