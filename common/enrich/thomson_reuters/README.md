# Thomson Reuters Pipelets
This directory houses the most current versions of all the Thomson Reuters pipelets.

### Pipelet List
* `reuters_complete.py` - The complete Open Calais enrichment piplet. Handles both adding the tags and performing the post-processing
* `reuters_tagging.py` - Split version of the reuters_complete pipelet that only adds tags from the Open Calais API.
* `tr.py` - Split version of the reuters_complete pipelet that only performs the post-processing. This pipelet does the followign:
  * Creates the derived facets
  * Removes Reuters tags (if applicable)
  * Adds tags to body (to be used with the Result Hilighting custom widget)

In general, new changes are made in `reuters_complete.py` first, so the other two pipelets may be out of date.

### Facets
A listing of _most_ of the facets created by the pipelets can be found in `facets.csv`
