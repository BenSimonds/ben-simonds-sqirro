"""
This pipelet cleans out bad values facets.
Use the 'query' parameter in the pipelet rerun
script to select a set of documents to cleanup
"""

from squirro.sdk import PipeletV1, require

@require('log')
class TagCleaningPipelet(PipeletV1):

    def __init__(self, config):

        self.config = config

        if not config.get('facets') or not config.get('values'):
            raise ValueError('Missing Facets or Values in Configuration')


    def consume(self, item):

        self.clean_tags(item)

        item['keywords']['tag_cleanup'] = ['1']
        return item


    def clean_tags(self, item):

        facets_to_clean = self.config.get('facets')
        bad_values = self.config.get('values')

        for facet in facets_to_clean:

            if not item['keywords'].get(facet):
                continue

            facets_to_remove = []

            for value in bad_values:

                while value in item['keywords'].get(facet, []):

                    item['keywords'][facet].remove(value)

                    if not item['keywords'].get(facet, []):
                        facets_to_remove.append(facet)

        for empty_facet in facets_to_remove:
            del item['keywords'][empty_facet]

        return item
