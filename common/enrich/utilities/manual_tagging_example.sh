#!/bin/bash

set -e
source ../../config/config.sh

pipelet -vv rerun \
    --token $TOKEN \
    --cluster $CLUSTER \
    --project-id $PROJECT_ID \
    --query 'Squirro' \
    --config '{"facets": ["Company"], "values": ["Squirro"]}' \
    'manual_tagging.py'
