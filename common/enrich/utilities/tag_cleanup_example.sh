#!/bin/bash

set -e
source config.sh

pipelet -vv rerun \
    --token $TOKEN \
    --cluster $CLUSTER \
    --project-id $PROJECT_ID \
    --query 'Squirro' \
    --config '{"facets": ["Name", "Location", "Age"], "values": ["Error", "Warning"]}' \
    'tag_cleaning.py'
