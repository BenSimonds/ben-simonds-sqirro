#!/usr/bin/env python
"""
Rerun all the enrichments on the project.

This uses the config from `main.ini` by default, but can be overwritten on the
command line.
"""
import logging
import os
import sys


# Get the helpers (adjust path to point to the `common/lib` folder)
sys.path.append(os.path.join(os.path.dirname(__file__), '../../lib'))
import project_config  # noqa


# Set up logging.
log = logging.getLogger('{0}[{1}]'.format(os.path.basename(sys.argv[0]),
                                          os.getpid()))


def run():
    """Main entry point run by __main__ below. No need to change this usually.
    """
    args = parse_args()
    project_config.setup_logging(args)
    config = project_config.get_config(args.config_file)
    args = project_config.update_args(config, args)

    log.debug('Arguments: %r', args)

    squirro_client = project_config.get_client(config)

    # run the application
    try:
        main(args, config, squirro_client)
    except Exception:
        log.exception('Processing error')


def main(args, config, squirro_client=None):
    """
    The main method. Any exceptions are caught outside of this method and will
    be handled.
    """
    project_id = config.get('squirro', 'project_id')
    enrichments = squirro_client.get_enrichments(project_id)
    for enrichment in enrichments:
        if enrichment['type'] == 'keyword':
            rerun_enrichment(squirro_client, project_id, enrichment)


def rerun_enrichment(client, project_id, enrichment):
    log.info('Rerunning for enrichment %r', enrichment['name'])
    query = enrichment['config']['query']
    items = get_items(client, project_id, query)
    for idx, item in enumerate(items):
        logging.debug('[%d/%d] $item_id:%s %s', idx + 1, len(items),
                      item['id'], item['title'])
        add_keywords(client, project_id, item,
                     enrichment['config']['keywords'])


def get_items(client, project_id, query):
    log.info('Searching matching documents for %r...', query)

    items = []
    start = 0
    eof = False
    next_params = None

    while not eof:
        log.info('Get documents starting at %d', start)
        results = client.query(
            project_id, query=query, count=100,
            fields=['link', 'title', 'created_at', 'keywords'],
            next_params=next_params, options={'update_cache': False})
        if results['count'] == 0:
            break

        next_params = results['next_params']

        if 'start' not in next_params:
            eof = True
        else:
            start = results['next_params']['start']

        items.extend(results['items'])

    log.debug('Returning %d items', len(items))
    return items


def add_keywords(client, project_id, item, keywords):
    changes = False
    item_kw = item.get('keywords', {})
    for keyword, values in keywords.iteritems():
        for val in values:
            if val not in item_kw.get(keyword, []):
                changes = True
                item_kw.setdefault(keyword, []).append(val)

    if changes:
        log.info('Updating keywords for item %s', item['id'])
        client.modify_item(project_id=project_id, item_id=item['id'],
                           keywords=item_kw)


def parse_args():
    """Parse command line arguments."""
    parser = project_config.get_arg_parser()
    return parser.parse_args()


# This is run if this script is executed, rather than imported.
if __name__ == '__main__':
    run()
