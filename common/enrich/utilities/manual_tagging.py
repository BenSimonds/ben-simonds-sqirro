"""
This pipelet manaully adds tags to an item.
Use the 'query' parameter in the pipelet rerun
script to select a set of documents to tag
"""

from squirro.sdk import PipeletV1, require

@require('log')
class TagCleaningPipelet(PipeletV1):

    def __init__(self, config):

        self.config = config

        if not config.get('facets') or not config.get('values'):
            raise ValueError('Missing Facets or Values in Configuration')


    def consume(self, item):

        self.add_tags(item)

        item['keywords']['manual_tagging'] = ['1']
        return item


    def add_tags(self, item):

        facets_to_add = self.config.get('facets')
        tag_values = self.config.get('values')

        for facet in facets_to_add:

            if not item['keywords'].get(facet):
                item['keywords'][facet] = tag_values

            else:
                if isinstance(item['keywords'].get(facet), list):
                    for value in tag_values:
                        if value not in item['keywords'].get(facet):
                            item['keywords'][facet].append(value)

        return item
