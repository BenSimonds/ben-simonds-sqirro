"""
This pipelet enriches each incoming item with the most relevant features
extracted using the Squirro SmartFilter/Fingerprint engine.

This edition of the piplet is only working with the sql loader utility and
is optimized for performance in that setup.

This piplet will create smartfilter, but will not delete them.
Use the clean_filters.py script to purge past filters from the db.

This piplet also uses redis for caching based on the content to speed up
subsequent indexing runs. This redis db is not being cleansed atm.

Also, only the title and the summary field is used to train the smartfilters.
"""

from squirro.sdk import PipeletV1, require
from squirro_client import SquirroClient
from hashlib import sha256
from bs4 import BeautifulSoup, Tag
import json
import os
import errno
import time
import uuid
import redis

@require('log')
class FeaturePipelet(PipeletV1):

    def __init__(self, config):

        #endpoint of the squirro cluster
        self.cluster = config.get('cluster', 'http://localhost:81')
        self.redis_host = config.get('redis_host', 'localhost')
        self.redis_port = config.get('redis_port', 6379)
        self.redis_db = config.get('redis_index', 15)
        self.redis_password = config.get('redis_password', None)

        self.redis = redis.StrictRedis(
            host=self.redis_host,
            db=self.redis_db,
            password=self.redis_password)

        #squirro api token
        if not config.get('token'):
            raise Exception('Squirro API token is missing')
        else:
            self.token = config.get('token')

        #squirro project id
        if not config.get('project_id'):
            raise Exception('Squirro project_id is missing')
        else:
            self.project_id = config.get('project_id')

        #use caching?
        self.caching = config.get('caching', True)

        #min. feature count
        self.min_features = config.get('min_features', 5)

        #max. feature count
        self.max_features = config.get('max_features', 30)

        #wordcount/feature ratio
        self.features_ratio = config.get('features_ratio', 15)

        self.feature_count = 30

        #which keyword to populate
        self.keyword = config.get('keyword', 'Features')

        #connect to squirro
        self.client = SquirroClient(None, None, cluster=self.cluster)
        self.client.authenticate(refresh_token=self.token)

        self.common_terms = [
            'the',
            'be',
            'and',
            'of',
            'a',
            'in',
            'to',
            'have',
            'to',
            'it',
            'I',
            'that',
            'for',
            'you',
            'he',
            'with',
            'on',
            'do',
            'say',
            'this',
            'they',
            'at',
            'but',
            'we',
            'his',
            'from',
            'that',
            'not',
            'by',
            'she',
            'or',
            'as',
            'what',
            'go',
            'their',
            'can',
            'who',
            'get',
            'if',
            'would',
            'her',
            'all',
            'my',
            'make',
            'about',
            'know',
            'will',
            'as',
            'up',
            'one',
            'time',
            'there',
            'year',
            'so',
            'think',
            'when',
            'which',
            'them',
            'some',
            'me',
            'people',
            'take',
            'out',
            'into',
            'just',
            'see',
            'him',
            'your',
            'come',
            'could',
            'now',
            'than',
            'like',
            'other',
            'how',
            'then',
            'its',
            'our',
            'two',
            'more',
            'these',
            'want',
            'way',
            'look',
            'first',
            'also',
            'new',
            'because',
            'day',
            'more',
            'use',
            'no',
            'man',
            'find',
            'here',
            'thing',
            'give',
            'many',
            'well',
            'only',
            'those',
            'tell',
            'one',
            'very',
            'her',
            'even',
            'back',
            'any',
            'good',
            'woman',
            'through',
            'us',
            'life',
            'child',
            'there',
            'work',
            'down',
            'may',
            'after',
            'should',
            'call',
            'world',
            'over',
            'school',
            'still',
            'try',
            'in',
            'as',
            'last',
            'ask',
            'need',
            'too',
            'feel',
            'three',
            'when',
            'state',
            'never',
            'become',
            'between',
            'high',
            'really',
            'something',
            'most',
            'another',
            'much',
            'family',
            'own',
            'out',
            'leave',
            'put',
            'old',
            'while',
            'mean',
            'on',
            'keep',
            'student',
            'why',
            'let',
            'great',
            'same',
            'big',
            'group',
            'begin',
            'seem',
            'country',
            'help',
            'talk',
            'where',
            'turn',
            'problem',
            'every',
            'start',
            'hand',
            'might',
            'American',
            'show',
            'part',
            'about',
            'against',
            'place',
            'over',
            'such',
            'again',
            'few',
            'case',
            'most',
            'week',
            'company',
            'where',
            'system',
            'each',
            'right',
            'program',
            'hear',
            'so',
            'question',
            'during',
            'work',
            'play',
            'government',
            'run',
            'small',
            'number',
            'off',
            'always',
            'move',
            'like',
            'night',
            'live',
            'Mr',
            'point',
            'believe',
            'hold',
            'today',
            'bring',
            'happen',
            'next',
            'without',
            'before',
            'large',
            'all',
            'million',
            'must',
            'home',
            'under',
            'water',
            'room',
            'write',
            'mother',
            'area',
            'national',
            'money',
            'story',
            'young',
            'fact',
            'month',
            'different',
            'lot',
            'right',
            'study',
            'book',
            'eye',
            'job',
            'word',
            'though',
            'business',
            'issue',
            'side',
            'kind',
            'four',
            'head',
            'far',
            'black',
            'long',
            'both',
            'little',
            'house',
            'yes',
            'after',
            'since',
            'long',
            'provide',
            'service',
            'around',
            'friend',
            'important',
            'father',
            'sit',
            'away',
            'until',
            'power',
            'hour',
            'game',
            'often',
            'yet',
            'line',
            'political',
            'end',
            'among',
            'ever',
            'stand',
            'bad',
            'lose',
            'however',
            'member',
            'pay',
            'law',
            'meet',
            'car',
            'city',
            'almost',
            'include',
            'continue',
            'set',
            'later',
            'community',
            'much',
            'name',
            'five',
            'once',
            'white',
            'least'
        ]

    def consume(self, item):
        self._enrich(item)
        return item


    def _get_digest(self, body):
        return sha256(repr(body)).hexdigest()


    def _get_features(self, body):

        #define the temp. smartfilter name
        filter_name = "extract_%s" % (uuid.uuid4())

        #build the tmp. smartfilter
        item = {}
        item['lang'] = 'en'
        item['text'] = body

        data = {}
        data['config'] = {}
        data['title'] = filter_name
        data['config']['max_features_per_language'] = self.feature_count

        self.client.new_fingerprint(type='project', type_id=self.project_id,
                               name=filter_name, data=data)

        self.client.update_fingerprint_from_content('project', self.project_id, filter_name, [item])
        sf = self.client.get_fingerprint('project', self.project_id, filter_name)

        config = sf.get('config', {})
        features = config.get('features', {})

        res = []

        for lang, values in features.iteritems():
            feature_list = values

            for feature in feature_list:

                label = feature.get('label')

                if label.lower() in self.common_terms:
                    continue

                res.append(label)

        return res


    def _enrich(self, item):

       #prep the item
        body = "%s %s" % (item.get('title', ''), item.get('summary', ''))
        body = body.strip()

        if len(body) == 0:
            return

        #cleanse the body from any pre tags
        #we don't want code snippets for Now

        soup = BeautifulSoup(body)

        for tag in soup.find_all('pre'):
            tag.replaceWith(' ')

        for tag in soup.find_all('h2'):
            tag.replaceWith(' ')

        body = soup.get_text()

        #determine how many features we want to extract
        feature_count = int(len(body.split(" "))/self.features_ratio)

        #enforce the lower and upper limits
        if feature_count < self.min_features:
            feature_count = self.min_features
        elif feature_count > self.max_features:
            feature_count = self.max_features

        self.feature_count = feature_count

        #try the cache?
        if self.caching:
            features = self._lookup_cache(body)
        else:
            features = False

        if features:
            #cache hit!
            self.log.debug("feature cache hit!")
            pass
        else:
            self.log.debug("feature cache miss!")
            features = self._get_features(body)

        if len(features) > 0:
            keywords = item.setdefault('keywords', {})

            if self.keyword in keywords:
                keywords[self.keyword] += features
            else:
                keywords[self.keyword] = features

            #write cache entry
            if self.caching:
                self._write_cache(body, features)


        return


    def _lookup_cache(self, body):

        cache_key = self._cache_key(body)
        value = self.redis.get(cache_key)

        if value:
            return json.loads(value)
        else:
            return None

    def _write_cache(self, body, keywords):

        cache_key = self._cache_key(body)
        self.redis.setex(cache_key, 86400, json.dumps(keywords))

        return

    def _cache_key(self, body):
        """crates persistant key for redis caching"""

        digest_body = repr(body) + str(self.feature_count)
        return sha256(digest_body).hexdigest() + "_feature2"
