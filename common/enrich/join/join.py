from squirro.sdk import PipeletV1, require
import csv


@require('log')
class JoinPipelet(PipeletV1):
    """A pipelet that can join data from a different CSV file.

    This mostly makes sense for usage in the data loader.

    Configuration keys:

        - `join_file`: The CSV file where the data to be joined in is located.
        - `join_keyword`: The item keyword which is the foreign key into the
          join data. Each value in the keyword is used.
        - `join_key`: The key on which to join the data in the join file.
        - `target_key`: The key in the Squirro item into which the joined rows
          are stored.
    """
    def __init__(self, config):
        self.config = config
        for key in ['join_file', 'join_keyword', 'join_key', 'target_key']:
            if not config.get(key):
                raise ValueError('Missing configuration for {0}'.format(key))

        join_key = config['join_key']
        data = {}
        with open(config['join_file'], 'rb') as f:
            reader = csv.DictReader(f)
            for row in reader:
                data.setdefault(row[join_key], []).append(row)
        self.join_data = data

    def consume(self, item):
        kw = item.get('keywords')
        if not kw:
            return item

        for lookup_key in kw.get(self.config['join_keyword'], []):
            item.setdefault(self.config['target_key'], []).extend(
                self.join_data.get(lookup_key, []))

        return item
