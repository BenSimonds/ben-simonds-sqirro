AlchemyAPI Pipelets
===================


## Pipelet Listing
- Concept
- Geo Entity
- Sentiment
- Targeted Sentiment
- Taxonomy
- Text Extraction
  - Because this pipelet modifies the body content of the item, it must be run during the load process before the docuemnts are indexed
- Text and Entity Extraction
  - This pipelet also created a table a the end of the body listing all of the extracted entities. As a result, it must be run during load before the documents are indexed


API Key
-------

Note that each call costs money. Do not setup demos and let them run forever
as this can cost us a lot of money.

Add the following config snippet to provide the api key:

```json
{ "api_key": "abcd..."}
```

Caching
-------

To save money, all pipelets do cache the responses under `/tmp/alchemyapi/`
Note that the full response from alchemy is cached, not what is sent
to Squirro.
