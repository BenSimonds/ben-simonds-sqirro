"""
This pipelet enriches each incoming item with geo entity data form AlchemyAPI.
"""

from squirro.sdk import PipeletV1, require
from hashlib import sha256
import json
import os
import errno

API_URL = 'https://access.alchemyapi.com/calls/html/HTMLGetRankedNamedEntities'
ENTITY_TYPES = frozenset(['Country', 'Region', 'City'])

@require('requests')
@require('log')
class AlchemyApiGeoPipelet(PipeletV1):

    def __init__(self, config):
        if not 'api_key' in config:
            raise ValueError('Missing API key')

        self.config = config

    def consume(self, item):
        self._enrich(item)
        return item

    def _enrich(self, item):
        data = {
            'apikey': self.config['api_key'],
            'html': item['body'],
            'outputMode': 'json',
        }
        headers = {
            'Accept': 'application/json',
        }

        #try cache
        res_data = self._lookup_cache(data['html'])

        if not res_data:
            #cache miss

            res = self.requests.post(API_URL, headers=headers, data=data)

            if res.status_code > 200:
                self.log.warn('Got invalid status code from AlchemyAPI: %r', res)
                return

            try:
                res_data = res.json()
                self._write_cache(data['html'], res_data) #cache

            except ValueError:
                self.log.exception('Error parsing AlchemyAPI response')
                return

        keywords = item.setdefault('keywords', {})

        for entity_type in ENTITY_TYPES:
            keywords['Geo_%s' % (entity_type)] = []

        entity_data = res_data.get('entities')

        for entity in entity_data:

            if not entity.get('type') in ENTITY_TYPES:
                continue

            try:
                value = entity['disambiguated']['name']
            except Exception:
                value = entity.get('text')

            value = self._transform_entities(value)

            if value:
                keywords['Geo_%s' % (entity.get('type'))].append(value)

    def _transform_entities(self, in_str):

        if in_str == "Wells Fargo":
            return False #wells fargo not a city?

        return in_str

    def _lookup_cache(self, body, ):

        file_path = self._cache_file_name(body)
        path = os.path.dirname(file_path)
        try:
            os.makedirs(path)
        except OSError as ex:
            if ex.errno == errno.EEXIST:
                #folder already exists, no need to raise an exception
                pass
            else:
                self.log.exception('Could not create cache folder %s', path)
                raise ex

        try:
            with open(file_path, 'rb') as f:
                return json.load(f)
        except Exception:
            return None

    def _write_cache(self, body, keywords):
        file_path = self._cache_file_name(body)
        try:
            with open(file_path, 'wb') as f:
                json.dump(keywords, f)
        except Exception as ex:
            self.log.exception('Could not write cache file')

    def _cache_file_name(self, body):

        digest_body = API_URL + repr(body)
        digest = sha256(digest_body).hexdigest()
        path = os.path.join('/tmp/alchemyapi/', digest[:2], digest[2:4], digest[4:6])
        file_path = path + '/' + digest + '.json'
        return file_path
