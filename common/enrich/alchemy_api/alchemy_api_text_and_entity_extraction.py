"""
This pipelet extracts both entities and clean
web page content usingalchemyapi (watson)
Uses the article URL stored in item['link']
"""

from squirro.sdk import PipeletV1, require
from hashlib import sha256
import json
import os
import errno
from jinja2 import Template

API_URL = 'http://gateway-a.watsonplatform.net/calls/url/URLGetRankedNamedEntities'

@require('requests')
@require('log')
class AlchemyApiTextAndEntityExtractionPipelet(PipeletV1):

    def __init__(self, config):
        if not 'api_key' in config:
            raise ValueError('Missing API key')

        config.setdefault('exclude_list', [])

        if not isinstance(config.get('exclude_list'), list):
            raise ValueError('Exclude List must be a List')

        self.config = config

    def consume(self, item):
        self._enrich(item)
        return item

    def _enrich(self, item):

        data = {
            'apikey': self.config['api_key'],
            'url': item['link'],
            'sourceText': 'cleaned',
            'showSourceText': 1,
            'outputMode': 'json',
        }

        #try cache
        res_data = self._lookup_cache(data['url'])

        if not res_data:
            #cache miss

            res = self.requests.post(API_URL, data=data)

            if res.status_code > 200:
                self.log.warn('Got invalid status code from AlchemyAPI: %r', res)
                return

            try:
                res_data = res.json()
                self._write_cache(data['url'], res_data) #cache

            except ValueError:
                self.log.exception('Error parsing AlchemyAPI response')
                return

        # Extract the cleaned content of the web page
        cleaned_text = res_data.get('text')
        if cleaned_text:
            item['body'] = cleaned_text
            item['keywords']['clean_content'] = [cleaned_text]


        item.setdefault('keywords', {})

        # Extract Entities
        if res_data.get('entities'):
            for entity in res_data.get('entities'):

                # Add the entire JSON string for the entity
                # as a facet value
                item['keywords'].setdefault('entity_json', [])
                item['keywords']['entity_json'].append(json.dumps(entity))

                entity_type = entity.get('type')
                entity_name = entity.get('text')

                # Populate facet for that specific entity type
                item['keywords'].setdefault(entity_type, [])
                item['keywords'][entity_type].append(entity_name)

                # Populate facets used for rendering body template
                if entity_type not in self.config['exclude_list']:
                    item['keywords'].setdefault('entity_text', [])
                    item['keywords'].setdefault('entity_type', [])
                    item['keywords'].setdefault('entity_count', [])
                    item['keywords'].setdefault('entity_disambiguated_name', [])
                    item['keywords'].setdefault('entity_dbpedia_link', [])

                    item['keywords']['entity_type'].append(entity_type)
                    item['keywords']['entity_text'].append(entity_name)
                    item['keywords']['entity_count'].append(entity.get('count', '-'))

                    if entity.get('disambiguated'):
                        disambiguated_name = entity['disambiguated'].get('name')
                        dbpedia_link = entity['disambiguated'].get('dbpedia')
                        item['keywords']['entity_disambiguated_name'].append(disambiguated_name)
                        item['keywords']['entity_dbpedia_link'].append(dbpedia_link)
                    else:
                        item['keywords']['entity_disambiguated_name'].append('-')
                        item['keywords']['entity_dbpedia_link'].append('-')


        item['keywords']['alchemy_text'] = ['2']
        item['keywords']['alchemy_entities'] = ['2']

        # Render Body Template
        jinja_template = '''{{ item['keywords']['clean_content'][0] }}<br><br><br><h1>Entities Referenced:</h1><br><br><br><table><tr><td>Entity Type</td><td>Entity Text</td><td>Entity Count</td><td>Entity Disambiguated Name</td><td>Entity DBpedia Link</td></tr>{% for i in range(0, item['keywords']['entity_text']|length) %}<tr><td>{{ item['keywords']['entity_type'][i] }}</td><td>{{ item['keywords']['entity_text'][i] }}</td><td>{{ item['keywords']['entity_count'][i] }}</td><td>{{ item['keywords']['entity_disambiguated_name'][i] }}</td>{% if item['keywords']['entity_dbpedia_link'][i] != '-' %}<td><a href="{{ item['keywords']['entity_dbpedia_link'][i] }}">DBpedia Link</a></td>{% else %}<td>-</td>{% endif %}</tr>{% endfor %}</table>'''
        template = Template(jinja_template)
        entities_section = template.render(item=item)

        item['body'] = item['body'] + entities_section

        # Remove '-' facet values

        return item


    def _lookup_cache(self, body, ):

        file_path = self._cache_file_name(body)
        path = os.path.dirname(file_path)
        try:
            os.makedirs(path)
        except OSError as ex:
            if ex.errno == errno.EEXIST:
                #folder already exists, no need to raise an exception
                pass
            else:
                self.log.exception('Could not create cache folder %s', path)
                raise ex

        try:
            with open(file_path, 'rb') as f:
                return json.load(f)
        except Exception:
            return None

    def _write_cache(self, body, keywords):
        file_path = self._cache_file_name(body)
        try:
            with open(file_path, 'wb') as f:
                json.dump(keywords, f)
        except Exception as ex:
            self.log.exception('Could not write cache file')

    def _cache_file_name(self, body):

        digest_body = API_URL + repr(body)
        digest = sha256(digest_body).hexdigest()
        path = os.path.join('/tmp/alchemyapi/', digest[:2], digest[2:4], digest[4:6])
        file_path = path + '/' + digest + '.json'
        return file_path
