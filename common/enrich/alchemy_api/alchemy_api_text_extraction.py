"""
This pipelet used web page text extraction provided by
alchemyapi (watson) to retreive a cleaned version of
the web page referenced in item['link']
"""

from squirro.sdk import PipeletV1, require
from hashlib import sha256
import json
import os
import errno

API_URL = 'https://gateway-a.watsonplatform.net/calls/url/URLGetText'

@require('requests')
@require('log')
class AlchemyApiTextExtractionPipelet(PipeletV1):

    def __init__(self, config):
        if not 'api_key' in config:
            raise ValueError('Missing API key')

        self.config = config

    def consume(self, item):
        self._enrich(item)
        return item

    def _enrich(self, item):

        data = {
            'apikey': self.config['api_key'],
            'url': item['link'],
            'outputMode': 'json',
        }

        #try cache
        res_data = self._lookup_cache(data['url'])

        if not res_data:
            #cache miss

            res = self.requests.post(API_URL, data=data)

            if res.status_code > 200:
                self.log.warn('Got invalid status code from AlchemyAPI: %r', res)
                return

            try:
                res_data = res.json()
                self._write_cache(data['url'], res_data) #cache

            except ValueError:
                self.log.exception('Error parsing AlchemyAPI response')
                return

        cleaned_text = res_data.get('text')
        if cleaned_text:
            item['body'] = cleaned_text

        item['keywords']['alchemy_cleanup'] = ['1']

        return item


    def _lookup_cache(self, body, ):

        file_path = self._cache_file_name(body)
        path = os.path.dirname(file_path)
        try:
            os.makedirs(path)
        except OSError as ex:
            if ex.errno == errno.EEXIST:
                #folder already exists, no need to raise an exception
                pass
            else:
                self.log.exception('Could not create cache folder %s', path)
                raise ex

        try:
            with open(file_path, 'rb') as f:
                return json.load(f)
        except Exception:
            return None

    def _write_cache(self, body, keywords):
        file_path = self._cache_file_name(body)
        try:
            with open(file_path, 'wb') as f:
                json.dump(keywords, f)
        except Exception as ex:
            self.log.exception('Could not write cache file')

    def _cache_file_name(self, body):

        digest_body = API_URL + repr(body)
        digest = sha256(digest_body).hexdigest()
        path = os.path.join('/tmp/alchemyapi/', digest[:2], digest[2:4], digest[4:6])
        file_path = path + '/' + digest + '.json'
        return file_path
