"""
This pipelet enriches each incoming item with entities
and targeted sentiment form AlchemyAPI.
"""

from squirro.sdk import PipeletV1, require
from hashlib import sha256
import json
import os
import errno

API_URL = 'https://access.alchemyapi.com/calls/html/HTMLGetRankedNamedEntities'
ENTITY_TYPES = frozenset(['City', 'Company', 'Country', 'FieldTerminology', 'FinancialMarketIndex', 'JobTitle', 'Organization', 'PrintMedia', 'Product', 'Region', 'Technology'])

@require('requests')
@require('log')
class AlchemyApiTargetedSentimentPipelet(PipeletV1):

    def __init__(self, config):
        """Initializing the pipelet"""

        if not 'api_key' in config:
            raise ValueError('Missing API key')

        self.config = config

        #disable sentiment detection by default
        #it costs an additional transaction
        if self.config.get('sentiment'):
            self.config['sentiment'] = 1
        else:
            self.config['sentiment'] = 0


        #enable disambiguation by default, its good for quality
        disambiguate = self.config.get('disambiguate', 'not_set')
        if disambiguate == 'not_set':
            self.config['disambiguate'] = 1
        elif disambiguate:
            self.config['disambiguate'] = 1
        else:
            self.config['disambiguate'] = 0


    def consume(self, item):
        self._enrich(item)
        item['keywords']['targeted_sentiment_version'] = ['1']
        return item

    def _enrich(self, item):
        data = {
            'apikey': self.config['api_key'],
            'html': item['body'],
            'outputMode': 'json',
            'disambiguate': self.config.get('disambiguate'),
            'coreference': 1,
            'sentiment': self.config.get('sentiment')
        }
        headers = {
            'Accept': 'application/json',
        }

        #try cache
        cache_key = "%s_%s_%s" % (self.config.get('disambiguate'), self.config.get('sentiment'), data['html'])
        res_data = self._lookup_cache(cache_key)

        if not res_data:
            #cache miss

            res = self.requests.post(API_URL, headers=headers, data=data)

            if res.status_code > 200:
                self.log.warn('Got invalid status code from AlchemyAPI: %r', res)
                return

            try:
                res_data = res.json()
                self._write_cache(cache_key, res_data) #cache

            except ValueError:
                self.log.exception('Error parsing AlchemyAPI response')
                return

        keywords = item.setdefault('keywords', {})

        keywords['alchemy_entity'] = ["4"]

        #create the facets
        for entity_type in ENTITY_TYPES:
            keywords[entity_type] = []

        keywords['Positive Entities'] = []
        keywords['Negative Entities'] = []

        entity_data = res_data.get('entities')
        print entity_data

        for entity in entity_data:

            #we only process whitelisted entities
            if not entity.get('type') in ENTITY_TYPES:
                continue

            #filter out entities with low relevance
            relevance = entity.get('relevance', "0")

            if float(relevance) < 0.25:
                continue

            #use disambiguated entity name if its present
            disambiguated = entity.get('disambiguated')
            if disambiguated:
                value = disambiguated.get('name')
            else:
                value = entity.get('text')

            #transform / skip?
            self.log.warn('Value in: %s' % (value))
            value = self._transform_entities(value, entity.get('type'))
            self.log.warn('Value out: %s' % (value))


            #add entity it to the keywords
            if not value:
                continue

            #keywords[entity.get('type')].append(value)

            #add sentiment keywords
            sentiment = entity.get('sentiment')

            if not sentiment:
                continue

            sentiment_type = sentiment.get('type', 'neutral')

            if sentiment_type == 'positive':
                keywords['Positive Entities'].append(value)
            elif sentiment_type == 'negative':
                keywords['Negative Entities'].append(value)
            else:
                #we don't store neutral entities atm
                pass

        #cleanse any empty keywords
        for entity_type in ENTITY_TYPES:
            if len(keywords[entity_type]) == 0:
                del keywords[entity_type]


    def _transform_entities(self, in_str, entity_type):
        """This function can be used to change / delete
           undesired entities"""

        in_str = in_str.strip().strip(".")

        if entity_type == "Company":

            if in_str == "The Royal Bank of Scotland Group":
                return "Royal Bank of Scotland"

            if in_str == "RBS":
                return "Royal Bank of Scotland"

            if in_str == "The Royal Bank of Scotland":
                return "Royal Bank of Scotland"

            if in_str == "RBS N.V":
                return "Royal Bank of Scotland"

            if in_str == "UK Retail":
                return False

            if in_str == "Basel I":
                return False

            if in_str == "Basel II":
                return False

            if in_str == "Basel III":
                return False

            if in_str == "Basel IV":
                return False

            if in_str == "Business Services":
                return False

        if entity_type == "Country":

            if in_str == "US":
                return "United States"

            if in_str == "USA":
                return "United States"

            if in_str == "America":
                return "United States"

            if in_str == "USD":
                return False

            if in_str == "UK":
                return "United Kingdom"

            if in_str == "United Kingdom of Great Britain and Ireland":
                return "United Kingdom"

            if in_str == "Britain":
                return "United Kingdom"

            if in_str == "England":
                return "United Kingdom"

            if in_str == "Kingdom of the Netherlands":
                return "Netherlands"

            if in_str == "Bourbon Restoration":
                return False

            if in_str == "GBP":
                return False

            if in_str == "Asia":
                return False

            if in_str == "Asia/Pacific":
                return False

            if in_str == "CRE":
                return False

            if in_str == "Europe":
                return False

            if in_str == "EU":
                return False

            if in_str == "Empire of Japan":
                return "Japan"

            if in_str == "Kingdom of Scotland":
                return "Scotland"

            if in_str == "AMRO":
                return False

            if in_str == "G20":
                return False

            if in_str == "Irish Republic":
                return "Ireland"

            if in_str == "HBOS":
                return False


            if in_str == "British Isles":
                return False


            if in_str == "Americas":
                return False

            if in_str == "Euro gold and silver commemorative coins (Belgium)":
                return False

            if in_str.find("Ireland") != -1:
                return "Ireland"

            if in_str == "Kingdom of Spain (Napoleonic)":
                return "Spain"

        if entity_type == "City":
            if in_str == "City of London":
                return "London"

            if in_str == "LONDON":
                return "London"

            if in_str == "New York City":
                return "New York"

            if in_str == "securitisations":
                return False

            if in_str == "Manhatten":
                return "New York"

            if in_str == "New York, Lincolnshire":
                return "New York"

            if in_str == "Donegall Square East Belfast":
                return False

            if in_str == "Jacksonville, Florida":
                return "Jacksonville"

            if in_str == "Washington, D.C.":
                return "Washington"

            if in_str == "Plaza Providence Rhode Island":
                return False

            if in_str == "NASDAQ.com":
                return False

            if in_str == "hemscott.com":
                return False

            if len(in_str) < 4 or len(in_str) > 20:
                return False

        return in_str

    def _lookup_cache(self, body, ):
        """Tests if this item is already in the cache, and returns it"""

        file_path = self._cache_file_name(body)
        path = os.path.dirname(file_path)
        try:
            os.makedirs(path)
        except OSError as ex:
            if ex.errno == errno.EEXIST:
                #folder already exists, no need to raise an exception
                pass
            else:
                self.log.exception('Could not create cache folder %s', path)
                raise ex
        try:
            with open(file_path, 'rb') as f:
                self.log.warn('HIT!')
                return json.load(f)
        except Exception:
            self.log.warn('MISS!')
            return None

    def _write_cache(self, body, keywords):
        """Write a new result into the cache"""
        file_path = self._cache_file_name(body)
        try:
            with open(file_path, 'wb') as f:
                json.dump(keywords, f)
        except Exception as ex:
            self.log.exception('Could not write cache file')

    def _cache_file_name(self, body):
        """Generates a unique cache filename"""
        digest_body = API_URL + repr(body)
        digest = sha256(digest_body).hexdigest()
        path = os.path.join('/tmp/alchemyapi_targeted/', digest[:2], digest[2:4], digest[4:6])
        file_path = path + '/' + digest + '.json'
        return file_path
