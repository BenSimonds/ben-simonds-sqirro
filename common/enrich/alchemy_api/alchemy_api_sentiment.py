"""
This pipelet enriches each incoming item with sentiment data form AlchemyAPI.
"""

from squirro.sdk import PipeletV1, require
from hashlib import sha256
import json
import os
import errno
from datetime import datetime
import requests
import os.path


API_URL = 'https://access.alchemyapi.com/calls/html/HTMLGetTextSentiment'


@require('requests')
@require('log')
class AlchemyApiSentimentPipelet(PipeletV1):

    def __init__(self, config):

        if not 'api_key' in config:
            raise ValueError('Missing API key')

        self.config = config


    def consume(self, item):

        self._enrich(item)
        item['keywords']['sentiment_version'] = ['1']
        return item


    def _enrich(self, item):

        data = {
            'apikey': self.config['api_key'],
            'html': item['body'],
            'outputMode': 'json',
        }

        headers = {
            'Accept': 'application/json',
        }

        #try cache
        res_data = self.get_response(
            request=API_URL,
            cache_location='/tmp/response_cache/',
            timeout=None,
            http='POST',
            headers=headers,
            data=data
            )

        res_data = json.loads(res_data)

        sentiment = res_data.get('docSentiment')
        keywords = item.setdefault('keywords', {})
        if sentiment:
            keywords['Sentiment'] = [sentiment['type']]

            if sentiment.get('mixed') == '1':
                keywords['SentimentMixed'] = ['Yes']

            else:
                keywords['SentimentMixed'] = ['No']



    # THE API RESPONSE CACHING FRAMEWORK
    # The main method, This is what is called to get a remote cached API response
    def get_response(self, request, cache_location='/tmp/response_cache/', timeout=None, http='GET', headers=None, data=None):

        # Check the cache first, then if no cache, go to the API to get the folder data
        response_data = self._lookup_cache(request, headers, data, cache_location, timeout)

        # Cache Miss
        if not response_data:
            # Make API Request
            response = self.get_web_response(request, http, headers, data)

            try:
                # Write to cache after successful API response
                self._write_cache(request, headers, data, response, cache_location)

            except ValueError:

                # If writing the first time fails, it is useful to try
                # a second time with the encoding set
                try:
                    self._write_cache(request, headers, data, response.encode('utf8'), cache_location)

                except UnicodeEncodeError:
                    print 'Error writing the response to the cache'

        # Cache Hit
        else:
            # work with the res_data as if it were the API response
            response = response_data

        return response


    def get_web_response(self, request, http, headers, data):

        if http == 'GET':
            return requests.get(request, headers=headers, data=data).text

        elif http == 'POST':
            return requests.post(request, headers=headers, data=data).text

        else:
            return None


    def _lookup_cache(self, request, headers, data, cache_location, timeout):

        file_path = self._cache_file_name(request, headers, data, cache_location)
        path = os.path.dirname(file_path)

        try:
            os.makedirs(path)

        except OSError as ex:
            if ex.errno != errno.EEXIST:
                print 'Could not create cache folder %s', path
                return None

        try:

            # Try to get the last modified date of the file
            if (self.get_cache_age(file_path) < timeout) or (not timeout):
                with open(file_path, 'rb') as f:
                    return json.load(f)['response']

            else:
                return None

        except Exception:
            return None


    def get_cache_age(self, file_path):

        modified_time = self.get_modified_time(file_path)
        current_time = datetime.now()

        cache_age = current_time - modified_time
        age_days = cache_age.days
        age_seconds = cache_age.seconds
        # Calculate the total number of second elapsed
        total_age_seconds = age_seconds + (86400 * age_days)

        # Convert the total age in seconds into hours (rounded down)
        total_age_hours = total_age_seconds // 3600

        return total_age_hours



    def get_modified_time(self, file_path):

        time_stamp = os.path.getmtime(file_path)

        return datetime.fromtimestamp(time_stamp)


    def _write_cache(self, request, headers, data, response, cache_location):

        file_path = self._cache_file_name(request, headers, data, cache_location)
        json_object = {'cache-key': request, 'response': response}

        with open(file_path, 'wb') as f:
            json.dump(json_object, f)


    def _cache_file_name(self, request, headers, data, cache_location):

        digest_body = str({
            "request": repr(request),
            "headers": headers,
            "data": data
            })
        digest = sha256(digest_body).hexdigest()

        path = os.path.join(cache_location, digest[:2], digest[2:4], digest[4:6])
        file_path = path + '/' + digest + '.json'

        return file_path
