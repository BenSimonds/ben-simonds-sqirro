from squirro.sdk import PipeletV1, require


@require('log')
class KeywordFilterPipelet(PipeletV1):
    """Discard items that don't have any value for specified keyword.

    Config key:

        - keyword
    """
    def __init__(self, config):
        self.config = config
        self.keyword = config['keyword']

    def consume(self, item):
        if not item.get('keywords', {}).get(self.keyword):
            return

        return item
