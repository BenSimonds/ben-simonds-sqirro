#!/usr/bin/env python
"""
Export a dashboard to a json file
"""
import logging
import os
import sys
import json
from squirro_client import SquirroClient
from squirro_client import NotFoundError

# Get the helpers
sys.path.append(os.path.join(os.path.dirname(__file__), '../../lib'))
import project_config


# Set up logging.
log = logging.getLogger('{0}[{1}]'.format(
    os.path.basename(sys.argv[0]), os.getpid()))


def main(args, config):
    args = project_config.update_args(config, args, ['sync_file'], 'dashboard')

    with open(args.sync_file, 'r') as sync_file:
        dashboards = json.load(sync_file)

    client = SquirroClient(None, None, cluster=args.cluster)
    client.authenticate(refresh_token=args.token)

    for dashboard in dashboards:
        import_dashboard(args.project_id, client, dashboard)


def import_dashboard(project_id, client, dashboard):
    dashboard_id = dashboard.get("id")
    dashboard_title = dashboard.get("title")
    log.info('Importing Dashboard %r (id: %r) ...', dashboard_title,
             dashboard_id)

    old_dashboard = get_dashboard(client, project_id, dashboard_id,
                                  dashboard_title)

    if not old_dashboard or old_dashboard.get('_fixup'):
        # patch the existing code to match the new project
        if dashboard.get('search'):
            dashboard['search']['project_id'] = project_id
        if dashboard.get('widgets'):
            for widget in dashboard['widgets']:
                if widget.get('project_id'):
                    widget['project_id'] = project_id

        # delete the theme from the code
        if dashboard.get('theme_id'):
            del dashboard['theme_id']
        if dashboard.get('theme'):
            del dashboard['theme']

    if old_dashboard:
        log.info('Updating dashboard %r (ID %s)', old_dashboard.get('title'),
                 old_dashboard['id'])
        client.modify_dashboard(
            project_id,
            dashboard_id=old_dashboard['id'],
            title=dashboard_title,
            search=dashboard.get('search'),
            widgets=dashboard.get('widgets'),
            type=dashboard.get('type'),
            column_count=dashboard.get('column_count'),
            row_height=dashboard.get('row_height')
        )
    else:
        log.info('Creating dashboard %r', dashboard_title)
        client.new_dashboard(
            project_id,
            title=dashboard_title,
            search=dashboard.get('search'),
            widgets=dashboard.get('widgets'),
            type=dashboard.get('type'),
            column_count=dashboard.get('column_count'),
            row_height=dashboard.get('row_height')
        )


def get_dashboard(client, project_id, id, title):
    try:
        return client.get_dashboard(project_id, id)
    except NotFoundError:
        # check if dashboard with same name is there
        for dashboard in client.get_dashboards(project_id):
            if dashboard.get('title') == title:
                dashboard['_fixup'] = True
                return dashboard


def parse_args():
    """Parse command line arguments."""
    parser = project_config.get_arg_parser()
    parser.add_argument(
        '--sync-file', dest='sync_file',
        help='Input file for dashboard content (JSON) as produced by '
             'export_dashboard.py')
    return parser.parse_args()


# This is run if this script is executed, rather than imported.
if __name__ == '__main__':
    args = parse_args()
    project_config.setup_logging(args)
    config = project_config.get_config(args.config_file)

    log.debug('Arguments: %r', args)

    # run the application
    try:
        main(args, config)
    except Exception as e:
        log.exception('Processing error')
