#!/usr/bin/env python
"""
Export a dashboard to a json file
"""
import logging
import os
import sys
import json
from squirro_client import SquirroClient

# Get the helpers
sys.path.append(os.path.join(os.path.dirname(__file__), '../../lib'))
import project_config


# Set up logging.
log = logging.getLogger('{0}[{1}]'.format(
    os.path.basename(sys.argv[0]), os.getpid()))


def main(args, config):
    args = project_config.update_args(
        config, args,
        ['sync_file', 'dashboards'], 'dashboard')
    if not args.dashboards:
        args.dashboards = 'all'
    names = [d.strip() for d in args.dashboards.split(',')]

    # make it easy to look up
    log.debug('Exporting dashboards %r', names)

    client = SquirroClient(None, None, cluster=args.cluster)
    client.authenticate(refresh_token=args.token)
    dashboards = client.get_dashboards(args.project_id)

    if args.sync_file:
        sync_file = open(args.sync_file, 'w')
    else:
        sync_file = sys.stdout

    export = []
    for dashboard in dashboards:
        if 'all' in names or dashboard.get('title') in names:
            export.append(dashboard)

    if export:
        sync_file.write(json.dumps(export, indent=4))
    else:
        log.warn('Did not find any dashboards to export')


def parse_args():
    """Parse command line arguments."""
    parser = project_config.get_arg_parser()
    parser.add_argument('--dashboards',
                        help='Comma separated list of all the dashboards to '
                             'export. Set to "all" to export all dashboards'),
    parser.add_argument('--sync-file', dest='sync_file',
                        help='Output file for dashboard content (JSON)')
    return parser.parse_args()


# This is run if this script is executed, rather than imported.
if __name__ == '__main__':
    args = parse_args()
    project_config.setup_logging(args)
    config = project_config.get_config(args.config_file)

    log.debug('Arguments: %r', args)

    # run the application
    try:
        main(args, config)
    except Exception as e:
        log.exception('Processing error')
