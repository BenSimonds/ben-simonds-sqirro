## Shared Delivery Resources

### Usage

This repository is meant to be checked out as a submodule in client project
repositories. On the root level put it as `common`.

    git submodule add git@github.com:squirro/delivery.git common

Where possible invoke the script from the `common` folder directly (e.g. `config/config.sh` is to be sourced from right here). In other cases use symlinks, e.g. for data loader plugins.

### Folder Structure

The folders here are the same structure as we follow for projects.

The folders are based on the user interface as much as possible. So use that as a guide on where to put things. Example: Smart Filters go to "search" because that's where they are in the UI.

- config
- dashboard - Dashboard custom widgets.
- data - Data loader plugins and other data loading resources.
- deployment - System components used for deployments.
- docs - Documentation.
- enrich - Pipelets, anything else related to the enrichment process.
- insights - Components used for catalyst detection.
- integration - Integration with 3rd party systems.
- lib - Generic Python libraries. Libraries that are specific to a certain
  feature (e.g. dashboard) should probably go into those more specific
  folders.
- search - Scripts and helpers related to search and Smart Filters.
- templates - Templates for building projects
- testing
