"""
Load data from Salesforce.com into Squirro
"""
import hashlib
import logging

from simple_salesforce import Salesforce

from squirro.dataloader.data_source import DataSource

log = logging.getLogger(__name__)


class SalesforceSource(DataSource):
    """
    Salesforce.com Plugin for the sq_data_loader
    """

    def __init__(self):
        self.args = None
        self.skip_fields = ['timespent', 'workratio', 'progress',
                            'aggregateprogress', 'worklog', 'lastViewed']

    def connect(self, inc_column=None, max_inc_value=None):
        log.debug('Incremental Column: %r', inc_column)
        log.debug('Incremental Last Value: %r', max_inc_value)

        self.connection = Salesforce(
            username=self.args.salesforce_user,
            password=self.args.salesforce_password,
            security_token=self.args.salesforce_token,
            instance=self.args.salesforce_instance,
            instance_url=self.args.salesforce_instance_url,
            organizationId=self.args.salesforce_organization_id,
            sandbox=self.args.salesforce_sandbox,
            version=self.args.salesforce_api_version)

        self.query = self.args.salesforce_query

    def disconnect(self):
        """Disconnect from the source."""
        # Nothing to do
        pass

    def getDataBatch(self, batch_size):
        """
        Generator - Get data from source on batches.
        :returns a list of dictionaries
        """
        batch = []
        res = self.connection.query_all(self.query)

        for item in res.get('records', []):
            batch.append(self._transformItem(item))

            if len(batch) >= batch_size:
                yield batch
                batch = []

        if batch:
            yield batch

    def getJobId(self):
        """
        Return a unique string for each different select
        :returns a string
        """
        # Generate a stable id that changes if any of the main parameters
        m = hashlib.sha256()
        m.update(self.args.salesforce_user)
        m.update(self.args.salesforce_query)
        job_id = m.hexdigest()
        log.debug("Job ID: %s", job_id)
        return job_id

    def getSchema(self):
        """
        Return the schema of the dataset

        :returns a List containing the names of the columns retrieved from the
        source
        """
        query = self.query
        if 'LIMIT' not in query:
            query += ' LIMIT 1'
        res = self.connection.query(query)
        schema = {}
        if res.get('records'):
            rec = self._transformItem(res['records'][0])
            schema = rec.keys()
        log.debug('Schema: %r', schema)
        return schema

    def add_args(self, parser):
        """
        Add source arguments to the main arguments parser
        """
        source_options = parser.add_argument_group('Source Options')

        source_options.add_argument('--salesforce-user',
                                    help="Salesforce username. Note: For sandbox, add .<sandbox name> to the username")
        source_options.add_argument('--salesforce-password',
                                    help='Salesforce password')
        source_options.add_argument('--salesforce-token',
                                    help='Salesforce security token, mutually exclusive with --salesforce-token')
        source_options.add_argument('--salesforce-organization-id',
                                    help='Salesforce OrgID, mutually exclusive with --salesforce-token')
        source_options.add_argument('--salesforce-query',
                                    help='Salesforce SOQL query')
        source_options.add_argument('--salesforce-instance',
                                    help='Domain of your Salesforce instance, i.e. na1.salesforce.com, mutually exclusive with --salesforce-instance-url')
        source_options.add_argument('--salesforce-instance-url',
                                    help='Full URL of your instance i.e. https://na1.salesforce.com, mutually exclusive with --salesforce-instance')
        source_options.add_argument('--salesforce-sandbox', dest='salesforce_sandbox', action='store_true',
                                    help='True if you want to login to test.salesforce.com, False if you want to login to login.salesforce.com.')
        source_options.add_argument('--salesforce-api-version',
                                    help='Optionally set the SOAP API Version, e.g. "37.0". Defaults to the version set by simple_salesforce"')

        return parser

    def _transformItem(self, item):
        ret = dict(item)

        # Add a 'Link' field
        if 'Id' in ret:
            ret['Link'] = 'https://{0}/{1}'.format(
                self.connection.sf_instance, ret['Id'])

        # Add flat versions of all keys
        for key, value in item.iteritems():
            if isinstance(value, dict):
                for subkey, subvalue in value.iteritems():
                    ret[key + '.' + subkey] = subvalue
                    if subkey == 'Id':
                        link = 'https://{0}/{1}'.format(
                            self.connection.sf_instance, subvalue)
                        ret[key + '.Link'] = link
                        ret[key]['Link'] = link

        return ret
