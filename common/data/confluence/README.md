# Atlassian Confluence Squirro Data Loader DataSource

This is a plugin for the Squirro Data Loader that allows you to load data directly from any Confluence instance.

It requires the v2 Rest API.
See https://docs.atlassian.com/confluence/REST/latest/

## Usage

Place the confluence.py file on your disk, the use the following argument for sq_data_load.py

```
--source-script path/to/confluence.py
```

The datasource plugin will now add these additional arguments:

```
--confluence-url https://nektoon.atlassian.net/wiki \
--confluence-user joe \
--confluence-password 'secret!' \
--confluence-query 'project != "Nektoon"' \
--confluence-batch-size 100 \
```

The confluence-query argument takes any valid CQL query.

Note: Don't sort your results, the data loader will do that. Also don't do date restrictions this might break the incremental loading.
