import collections
import dateutil.parser
import hashlib
import json
import logging
import requests
import urllib2
import sys

from squirro.dataloader.data_source import DataSource

VERSION = '0.0.1'
log = logging.getLogger(__name__)


class ConfluenceSource(DataSource):
    """Squirro Data Loader `DataSource` used to upload Confluence data to
    Squirro."""

    def __init__(self):
        self.args = None

    def connect(self, inc_column=None, max_inc_value=None):
        """Create connection with the source."""

        log.debug('Incremental Column: %r', inc_column)
        log.debug('Incremental Last Value: %r', max_inc_value)

        self.requests = requests.Session()

        self.base_url = "{0}/rest/api/".format(self.args.confluence_url)
        log.info('Base URL: %s', self.base_url)

        cql_query = self.args.confluence_query

        # is this an incremental load?
        if max_inc_value:
            # parse the date
            max_inc_date = dateutil.parser.parse(max_inc_value)
            cql_date = max_inc_date.strftime('%Y/%m/%d %H:%M')

            # handle incremental loading based on what the query is
            if cql_query == "":
                cql_query = 'lastModified >= "{0}"'.format(cql_date)
            else:
                query_template = '( {0} ) AND lastModified >= "{1}"'
                cql_query = query_template.format(cql_query, cql_date)

        # extend the cql query with the order command
        # cql_query = '{0} ORDER BY lastModified asc'.format(cql_query)

        log.info('Running the initial query: %r', cql_query)

        cql_query = urllib2.quote(cql_query, '')

        query_template = 'search?expand=content.body.storage,content.history.contributors.publishers.users,content.ancestors&cql={0}&excerpt=indexed'
        self.query = query_template.format(cql_query)

        self.url = "{0}{1}".format(self.base_url, self.query)

        self.user = self.args.confluence_user
        self.pwd = self.args.confluence_password

        self.data = None

        # get the first batch
        self._query_confluence(self.url, 0, self.args.confluence_batch_size)

        if not self.data:
            raise Exception('No data found')

        log.info('Found %i wiki to index', self.data.get('size'))

        log.debug('Sample record: %s', json.dumps(self.data.get('results')[0], indent=4))

        #prepare the schema from the first row
        sample_data = self._transform_wiki_page(self.data.get('results')[0])
        self.schema = list(sample_data.keys())

    def disconnect(self):
        """Disconnect from Confluence not needed due to REST interface."""
        pass

    def getDataBatch(self, batch_size):
        """Get data from Confluence in batches.
        :returns a list of dictionaries"""

        while self.data:

            wiki_pages = []

            response_wiki_pages = self.data.get('results')

            if not response_wiki_pages:
                raise Exception('No data found to load!')

            for wiki_page in response_wiki_pages:
                transformed_wiki_pages = self._transform_wiki_page(wiki_page)

                if not transformed_wiki_pages:
                    log.debug('Skipping invalid wiki page')
                    continue

                wiki_pages.append(transformed_wiki_pages)

            yield wiki_pages

            # is there more data?
            total = self.data.get('totalSize')
            start_at = self.data.get('start')
            max_results = self.data.get('limit')

            log.info('exit condition start_at %r max_results %r total %r', start_at, max_results, total)
            if start_at + max_results >= total:
                log.info('done')
                self.data = None
            else:
                self._query_confluence(self.url, start_at + max_results, max_results)

    def getJobId(self):
        """Return a unique string for each different select
        :returns a string"""

        # generate a stable id that changes if any of the Confluence
        # parameters change

        m = hashlib.sha256()
        m.update(self.args.confluence_url)
        m.update(self.args.confluence_user)
        m.update(self.args.confluence_query)
        job_id = m.hexdigest()

        log.info("Job ID: %s", job_id)

        return job_id

    def getSchema(self):
        """Return the schema of the data set
        :returns a list of Confluence column names"""

        if self.schema:
            log.debug('Schema: %r', self.schema)

            log.debug("Columns available for templating and facets:")

            for column in sorted(self.schema):
                log.info("- %s", column)

            return self.schema
        else:
            log.warn('No schema')
            return []

    def add_args(self, parser):
        """Add source arguments to the main argument parser"""

        source_options = parser.add_argument_group("Source Options")

        source_options.add_argument('--confluence-url',
                                    help="Endpoint of the Confluence instance")
        source_options.add_argument('--confluence-user',
                                    help="Confluence username")
        source_options.add_argument('--confluence-password',
                                    help='Confluence password')
        source_options.add_argument('--confluence-query',
                                    help='Confluence CQL query, Default: ""')
        source_options.add_argument('--confluence-batch-size', default=100, type=int,
                                    help='How many items are fetched per' \
                                         'REST transaction. Default: 100')

        return parser

    def _query_confluence(self, url, start=0, batch_size=10):
        """Query data from Confluence, store it into self.data"""

        full_url = "{0}&limit={1}&start={2}" \
            .format(url, batch_size, start)

        log.info("Sending Request: %r", full_url)

        self.result = self.requests.get(full_url, auth=(self.user, self.pwd))

        if self.result.status_code != 200:
            log.error('Error %i returned by Confluence api: %r',
                      self.result.status_code, self.result.text)

            self.data = None
        else:
            self.data = self.result.json()

    def _get_date(self, date_string):
        """Turns ISO-8601 et al into a python datetime object"""
        return dateutil.parser.parse(date_string).strftime('%Y-%m-%dT%H:%M:%S')

    def _transform_wiki_page(self, page):
        """Transforms a single confluence entry from rest format
        into a flat list of columns suitable for the data loader."""

        # log.info("Wiki data before transformation: %s",
        #          json.dumps(page, indent=4))

        columns = {}

        columns['wiki'] = self.args.confluence_url
        columns['space'] = page['resultGlobalContainer']['title']

        columns['ancestors'], sep = '', ''
        for ancestor in page['content']['ancestors']:
            columns['ancestors'] = sep + ancestor['title']


        content = page.get('content')

        for field in ['id', 'title', 'status', 'type']:
            if field in content:
                columns[field] = content.get(field)
                if field == 'id':
                    columns['key'] = content.get(field)
                elif field == 'type':
                    if content.get(field) == 'page':
                        columns['type'] = 'Page'
                    elif content.get(field) == 'attachment':
                        columns['type'] = 'Document'
                    elif content.get(field) == 'comment':
                        columns['type'] = 'Comment'
                    else:
                        columns['type'] = content.get(field)


        for field in ['lastModified', 'url', 'excerpt', 'entityType']:
            if field in page:

                if field == 'lastModified':
                    columns['updated'] = self._get_date(page.get(field))
                elif field == 'url':
                    columns['url'] = self.args.confluence_url + page.get('url')
                else:
                    columns[field] = page.get(field)

        # extract wiki page body
        # requires expand=content.body.storage
        if 'body' in content:
            body = content.get('body')
            if 'storage' in body:
                storage = body.get('storage')
                if 'value' in storage:
                    html_body = storage.get('value')
                    columns['body'] = html_body
                    columns['summary'] = html_body

        # extract creator and created
        # requires expand=content.history
        if 'history' in content:
            history = content.get('history')
            if 'createdBy' in history:
                created_by = history.get('createdBy')
                if 'displayName' in created_by:
                    columns['creator_displayName'] = created_by.get('displayName')
            if 'createdDate' in history:
                columns['created'] = self._get_date(history.get('createdDate'))

            # extract participants
            # requires expand=content.history.contributors.publishers.users
            if 'contributors' in history:
                contributors = history.get('contributors')
                if 'publishers' in contributors:
                    publishers = contributors.get('publishers')
                    if publishers is not None and 'users' in publishers:
                        users = publishers.get('users')

                        for user in users:
                            if 'displayName' in user:
                                if 'contributors' in columns:
                                    columns['contributors'] += ';' + user.get('displayName')
                                else:
                                    columns['contributors'] = user.get('displayName')

        if 'extract' not in columns:
            columns['extract'] = '(no extract)'

        # mediaType for attachments
        if 'metadata' in content:
            metadata = content.get('metadata')
            if metadata is not None and 'mediaType' in metadata:
                mediaType = metadata.get('mediaType')
                if mediaType is not None:
                    columns['mediaType'] = mediaType

        # order columns for readability
        columns = collections.OrderedDict(sorted(columns.items()))

        if not columns.get('summary') or columns.get('summary') == '':
            excerpt = page.get('excerpt')
            if excerpt and excerpt != '':
                columns['summary'] = excerpt
            else:
                columns['summary'] = page.get('title')

        columns['names'] = columns['contributors']

        # log.info("Wiki data after transformation: %s",
        #           json.dumps(columns, indent=4))


        return columns
