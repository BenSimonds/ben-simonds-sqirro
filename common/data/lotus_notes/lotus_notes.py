# -*- coding: utf-8 -*-
"""Data source implementation for emails in Lotus Notes.

Usage:

    Two arguments are accepted for this loader.

    --source-server - Server on which the file is. Keep this empty for a local
      .nsf file.
    --source-path - NSF file name to import.
    --output-type - whether to output the emails or the attachments. Valid
      values are `mails` - which is the default - and `attachments`. To import
      both, the loader needs to be executed twice.

    See example.sh for how to use this in anger.

Rows have the following keys:

    - type: Email or Attachment (see --output-type below)
    - body
    - mime_type: The MIME type of the body (for type Email) or the attachment
      (for type Attachment)
    - subject
    - created_at
    - recipients: all recipient email addresses (To, Cc) - separated by |
    - recipient_names: all recipient names (To, Cc) - separated by |
    - sender: sender email address - separated by |
    - sender_name: sender name - separated by |
    - Additionally each message header is made available as well with the
      lower-case name of the header as the key

Attachments also have the following keys:

    - id: An ID that combines both the message_id and the attachment_id into a
      unique identifier
    - filename
    - data: The actual data of the attachment
"""
import datetime
import hashlib
import win32com.client
import csv
import sys
import re
import logging
import os
import shutil
import subprocess
import tempfile
import uuid

import pytz
from squirro.dataloader.data_source import DataSource

log = logging.getLogger(__name__)

# Keys that can be returned from an email - this only lists the hard-coded
# ones. In addition to these any header is returned as well.
KEYS = frozenset([
    'subject', 'body', 'mime_type', 'created_at', 'from', 'to', 'message-id',
    'recipients', 'recipient_names', 'sender', 'sender_name',
])

ATTACHMENT_KEYS = frozenset([
    'id', 'filename', 'data',
])

# MIME types for which we import attachments. This still has to be extended a
# bit.
MIME_TYPES = [
    'application/pdf', 'text/pdf', 'text/html',
]


class EmailSource(DataSource):
    def __init__(self):
        self.args = None

    def connect(self, inc_column=None, max_inc_value=None):
        """Create connection with the source."""
        self.session = win32com.client.Dispatch('Lotus.NotesSession')
        self.session.Initialize(self.args.notes_password)
        self.db = self.session.GetDatabase(
            self.args.source_server, self.args.source_path)

    def disconnect(self):
        """Disconnect from the source."""
        pass

    def getDataBatch(self, batch_size):
        """
        Generator - Get data from source on batches.

        :returns a list of dictionaries
        """
        items = []
        for view in self.db.Views:
            for item in self._get_items(view.Name):
                item['$view'] = view.Name
                if not item.get('subject'):
                    log.warn('Ignoring item without subject: %r', item.get('message-id'))
                else:
                    items.append(item)
                    if len(items) >= batch_size:
                        yield items
                        items = []

        if items:
            yield items

    def getJobId(self):
        """
        Return a unique string for each different select

        :returns a string
        """
        paths = [self.args.source_server, os.getcwd(), self.args.source_path]
        return ','.join(sorted(paths))

    def _get_items(self, view_name):
        log.debug('Getting items for view %s', view_name)
        if view_name != '(All)':
            return

        view = self.db.GetView(view_name)
        if not view:
            raise Exception('View "%s" not found' % view_name)

        # Get the first document
        document = view.GetFirstDocument()
        while document:
            # Yield it
            if document.GetItemValue('Form')[0] != 'REQ':
                log.debug('Skipping document of type %s',
                          document.GetItemValue('Form')[0])
            else:
                yield self._get_item(document)

            # Get the next document
            document = view.GetNextDocument(document)

    def _get_item(self, document):
        # Documentation for the document (type NotesDocument):
        # http://www-12.lotus.com/ldd/doc/lotusscript/lotusscript.nsf/1efb1287fc7c27388525642e0074f2b6/c4a6af5a62ba9d0b8525642e007687d5?OpenDocument

        ret = {}

        # TODO: there may be message IDs in the document. Alternatively change
        # this to "id".
        ret['message-id'] = document.UniversalID.strip()

        ret['created_at'] = self._parse_date(document.Created)

        # Confusingly Lotus Notes calls their properties on a document "Items".
        # Don't confuse this with the Squirro item we are creating for the
        # document as a whole.
        for notes_item in document.Items:
            ret[notes_item.Name.lower()] = notes_item.Text
            ret[notes_item.Name] = notes_item.Text

        # import pprint
        # pprint.pprint(ret)
        return ret

    def _parse_date(self, created):
        # Created is of type PyTime
        return created.Format('%Y-%m-%dT%H:%M:%S')

    def getSchema(self):
        """
        Return the schema of the data set

        :returns a List containing the names of the columns retrieved from the
            source
        """
        ret = KEYS
        if self.args.output_type == 'attachments':
            ret = ret | ATTACHMENT_KEYS
        return ret

    def add_args(self, parser):
        """
        Add source arguments to the main arguments parser
        """
        source_options = parser.add_argument_group("Source Options")
        source_options.add_argument(
            '--source-server', default='',
            help='Lotus Notes server on which to find the source file. Leave '
                 'empty for local files.')
        source_options.add_argument(
            '--source-path', required=True,
            help='Path of the Lotus Notes .nsf email file.')
        source_options.add_argument(
            '--notes-password', default='',
            help='Lotus Notes password.')

        source_options.add_argument(
            '--output-type', default='mails', choices=['mails', 'attachments'],
            help='Whether to emit the emails or the attachments. '
                 'Default: %(default)s.')

        return parser
