# Facet Setup

## Editing Facets.json Directly
The `facets.json` file here can be directly added, by following the documentation available at https://squirro.atlassian.net/wiki/display/DOC/Config+Reference

## Editing Facets using a spreadsheet
To edit facets using a spreadsheet, the python scripts included here can be used to convert back and forth between CSV and JSON files.

To get started, run `create_spreadsheet.sh` to create a `facets.csv` file of the facets.
Once changes are made, the script `update_facets.sh` can be run to create a new `facets.json` file based on the contents of `facets.csv`
