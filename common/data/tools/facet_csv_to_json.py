import csv
import json

with open('facets.csv', 'r') as csvfile:

    csv_data = csv.DictReader(csvfile)
    all_facets = {}

    for row in csv_data:

        all_facets[row['facet']] = {}
        facet_name = row['facet']
        del row['facet']

        for key, value in row.iteritems():

            if value in ['TRUE', 'FALSE']:

                if value == 'TRUE':
                    all_facets[facet_name][key] = True

                else:
                    all_facets[facet_name][key] = False

            elif value != '':
                all_facets[facet_name][key] = value

with open('facets.json', 'wb') as jsonfile:

    json.dump(all_facets, jsonfile)
