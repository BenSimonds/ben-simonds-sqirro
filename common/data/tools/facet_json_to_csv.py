'''
Utility for converting facets.json into a CSV file for easy editing
'''

import csv
import json


# Helper for getting all field names used in the json file
def get_all_field_names(json_data):

    # Start with the column label which holds the name of the facet
    all_field_names = ['facet']

    for facet_name, facet_params in json_data.iteritems():
        for param_name, param_value in facet_params.iteritems():

            if param_name not in all_field_names:
                all_field_names.append(param_name)

    return all_field_names


# Main Process
def process_file(filename):

    csv_filename = filename[:-5] + '.csv'

    with open(filename) as json_file:

        json_data = json.load(json_file)
        field_names = get_all_field_names(json_data)

        with open(csv_filename, 'w') as csvfile:

            writer = csv.DictWriter(csvfile, fieldnames=field_names)
            writer.writeheader()

            for facet_name, facet_params in json_data.iteritems():

                data_row = {}
                data_row['facet'] = facet_name

                for param_name, param_value in facet_params.iteritems():

                    data_row[param_name] = param_value

                writer.writerow(data_row)

# Check for facet file manifest
try:
    with open('facet_manifest.txt') as manifest_file:
        manifest_data = manifest_file.readlines()
        for line in manifest_data:
            process_file(line.strip())

except IOError:
    process_file('facets.json')
