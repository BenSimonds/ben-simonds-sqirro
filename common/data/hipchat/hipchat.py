# -*- coding: utf-8 -*-
"""HipChat plugin.

This plugin implements a config file for some of its options.
"""
import ConfigParser
import datetime
import hashlib
import logging
import os
import re

import hypchat
from squirro.dataloader.data_source import DataSource

log = logging.getLogger(__name__)


class HipchatSource(DataSource):
    """
    Load messages from rooms in Hipchat

    Has two modes of operation;

    - message: in this mode is just pulls messages from today (via the
        HipChat "recent" API call) and pushs them to Squirro as individual
        documents. This is good for "real time" updates
    - conversation: this mode groups messages into conversations, by grouping
        messages posted around the same time. The hipchat-conversation-length
        and hipchat-conversation-age arguments can be used to tune it
    """

    def __init__(self):
        self.load_config()
        self.load_blacklist()
        self.emoticon_re = None

    def connect(self, inc_column=None, max_inc_value=None):
        """
        Setup HipChat connection plus extracting config options
        """
        log.debug('Connecting to HipChat...')

        def get_config_value(name, errmsg):
            value = None
            if hasattr(self.args, "hipchat_" + name):
                log.debug("Fetching %s from args" % name)
                value = getattr(self.args, "hipchat_" + name)
            elif self.config.has_option('hipchat', name):
                log.debug("Fetching %s from ini" % name)
                value = self.config.get('hipchat', name)
            else:
                raise Exception(errmsg)
            return value

        authtoken = get_config_value(
            "authtoken",
            "HipChat API auth_token required"
        )

        self.connection = hypchat.HypChat(authtoken)

        self.hipchat_url = get_config_value(
            "url",
            "HipChat URL required"
        )

        self.conversation_length = get_config_value(
            'conversation_length', 'conversation_length not defined') * 60 * 60

        self.conversation_age = get_config_value(
            'conversation_age', 'conversation_ago not defined')

    def disconnect(self):
        """Disconnect from the source."""
        pass

    def getJobId(self):
        """
        Return a unique string for each diffrent select
        """
        log.debug('function getJobId')
        digest = hashlib.md5()
        digest.update("HipChat_" + str(datetime.datetime.now()))
        return digest.hexdigest()

    def getSchema(self):
        """
        Return the schema of the dataset
        """
        return ['id', 'title', 'body', 'date', 'mentions']

    def getDataBatch(self, batch_size):
        """
        Get data from the resultset using a generator
        """
        data = []

        if self.args.hipchat_mode == 'conversation':
            for conversation in self.past_conversations():
                data.append(conversation)
                if len(data) == batch_size:
                    yield data
                    data = []

        else:
            for message in self.todays_messages():
                data.append(self.transform_message(message))
                if len(data) == batch_size:
                    yield data
                    data = []

        yield data

    def transform_message(self, message):
        """
        Re-structures and resolves participants and emoticons
        """
        t_message = self.transform_message_payload(message)

        participants = []
        t_message['participants'] = self.delimited_participants(
                self.extract_participants(message, participants)
            )

        t_message['names'] = t_message['participants']

        t_message['emoticons'] = self.delimited_emoticons(
                self.match_emoticons(message['message'])
            )

        return t_message

    def transform_message_payload(self, message):
        """
        Massage the message
        """
        sender = self.extract_sender(message)

        t_message = {
            'id': message['id'],
            'grouping': 'Message',
            'title': "%s posted message to %s at %s" % (
                sender,
                message['room_name'],
                message['date']),
            'body': message['message'],
            'date': message['date'],
            'summary': message['message'],
            'link': message['room_link'],
            'room': message['room_name'],
            'participants': [],
            'emoticons': ''
        }

        return t_message

    def extract_sender(self, message):
        """
        HipChat sometimes returns string names in 'from' instead of
        user objects when it's not a real user e.g. Twitter Bot
        Returns the sender name as a string
        """
        sender = message['from']

        if type(message['from']) == hypchat.restobject.User:
            sender = message['from']['name']

        return sender

    def extract_participants(self, message, participants):
        """
        Returns a unique list of participants in a message, including
        the sender
        """
        sender = self.extract_sender(message)
        if sender not in participants:
            participants.append(sender)

        for mention in message['mentions']:
            if mention['name'] not in participants:
                participants.append(mention['name'])

        return participants

    def delimited_participants(self, participants):
        """
        Converts a list of participants into a delimited string
        """
        p_string, sep = "", ""
        for name in participants:
            p_string += sep + name
            sep = ";"
        return p_string

    def delimited_emoticons(self, emoticons):
        """
        Generates delimited string from list of emoticons using ! as
        delimiter and stripping brackets which trip search UI
        """
        e_string, sep = "", ""
        for emoticon in emoticons:
            # Workaround for UI bug handling brackets
            if emoticon[0] == "(" and emoticon[-1] == ")":
                emoticon = emoticon[1:-1]
            e_string += sep + emoticon
            sep = "!"
        return e_string

    def todays_messages(self):
        """
        Gets all messages that were posted today and handles then 1 by 1
        """
        rooms = self.connection.rooms(expand='items')

        messages = []
        for room in rooms['items']:
            if not self.room_ok(room):
                continue

            if self.older_than(room['last_active'],
                               datetime.datetime.now().hour):
                continue

            for item in self.recent_messages(room):
                item['room_name'] = room['name']
                item['room_link'] = "%s/rooms/show/%s" % (self.hipchat_url,
                                                          room['id'])
                messages.append(item)

        return messages

    def past_conversations(self):
        """
        Gets past messages and groups them into conversations
        """
        rooms = self.connection.rooms(expand='items')

        conversations = []

        for room in rooms['items']:
            if not self.room_ok(room):
                continue

            last_message_time = None
            messages = []
            for item in self.message_archive(
                    room, self.past_day(self.conversation_age + 1)):

                # End of conversation...
                if last_message_time and (item['date'] - last_message_time).total_seconds() > self.conversation_length:

                    starter, started, summary = None, None, None
                    participants, emoticons = [], []
                    body, sep = "", ""
                    for message in messages:
                        sender = self.extract_sender(message)

                        if not starter:
                            starter = sender
                            started = message['date']
                            summary = message['message']

                        body += sep + "<strong>" + sender + "</strong>"
                        body += " at " + message['date'].strftime('%I:%M%p')
                        body += "<br/>" + message['message']
                        sep = "<hr/>"
                        participants = self.extract_participants(
                            message, participants)
                        for emoticon in self.match_emoticons(message['message']):
                            if emoticon not in emoticons:
                                emoticons.append(emoticon)

                    title = "%s started a conversation in %s at %s" % (
                        starter,
                        room['name'],
                        started)

                    conversation = {
                        'id': hashlib.md5(title).hexdigest(),
                        'grouping': 'Conversation',
                        'title': title,
                        'body': body,
                        'date': started,
                        'summary': summary,
                        'link': "%s/rooms/show/%s" % (
                            self.hipchat_url, room['id']),
                        'room': room['name'],
                        'participants': self.delimited_participants(
                            participants),
                        'emoticons': self.delimited_emoticons(emoticons),
                    }

                    conversation['names'] = conversation['participants']

                    conversations.append(conversation)

                    messages, participants, emoticons = [], [], []

                last_message_time = item['date']
                messages.append(item)

        return conversations

    def room_ok(self, room):
        """
        Rooms which are private or on the blacklist return False
        """
        if room['privacy'] == 'privacy':
            return False
        if room['name'] in self.room_blacklist:
            return False
        return True

    def add_args(self, parser):
        """
        Add source arguments to the main argumens parser
        """
        source_options = parser.add_argument_group("Source Options")
        source_options.add_argument('--hipchat-url',
                        help="URL of HipChat public web interface")
        source_options.add_argument('--hipchat-endpoint',
                        help="Endpoint of the HipChat API (URL)")
        source_options.add_argument('--hipchat-authtoken',
                        help="Authentication token for HipChat API")
        source_options.add_argument('--hipchat-conversation-length',
                        type=int,
                        default=3,
                        help="Number of hours silence after which a conversation is considered ended")
        source_options.add_argument('--hipchat-conversation-age',
                        type=int,
                        default=1,
                        help="Number of days back into the past for which to fetch the message archive counting back from yesterday"
                        )
        source_options.add_argument('--hipchat-mode',
                        default="message",
                        choices=["message","conversation"],
                        help="Whether to individual messages (realtime) or groups conversations (older than 1 day")
        return parser

    def load_config(self):
        """
        Loads configuration from a file hipchat_config.ini located
        in the same directory as this script
        """
        config_fn = 'hipchat_config.ini'

        log.info("Reading config from %s" % config_fn)
        self.config = ConfigParser.SafeConfigParser()
        self.config.read(config_fn)

    def load_blacklist(self):
        """
        Load the blacklist of rooms to ignore
        """
        blacklist_fn = 'hipchat_room_blacklist'

        log.info("Reading blacklist from %s" % blacklist_fn)

        self.room_blacklist = []
        if os.path.exists(blacklist_fn):
            with open(blacklist_fn) as f:
                for line in f:
                    room_name = line.split('#')[0].strip()
                    self.room_blacklist.append(room_name)

    def recent_messages(self, room):
        """
        Returns messages from today for a given room
        """
        return self.message_history(
            room,
            startDate='recent',
            endDate=self.yesterday(),
            reverse=False
        )

    def message_archive(self, room, endDate='null'):
        """
        Get messages older than today for a given room

        Right after midnight HipChat will continue to complain like

        {
          "error": {
            "code": 400,
            "message": "This day has not yet come to pass",
            "type": "Bad Request"
          }
        }

        ... need to delay "yesterday"
        """
        startDate = self.yesterday()

        # Seems it takes at least 2 hours for yesterdays index to be
        # created by the HipChat servers. Otherwise get the error
        # "This day has not yet come to pass"
        if datetime.datetime.now().hour < 2:
            startDate = startDate - datetime.timedelta(days=1)
            if type(endDate) == datetime.datetime:
                endDate = endDate - datetime.timedelta(days=1)

        return self.message_history(
            room,
            startDate = startDate,
            endDate = endDate,
            reverse = True
        )

    def message_history(self, room, startDate, endDate, reverse):
        """
        Return the history of a room.

        Has two modes:
        - recent: returns only messages from today
        - archive: returns up to 1000 messages until yesterday

        The HypChat library doesn't provide enough control over requests
        to the Hipchat room history API so need to access "private" members

        See https://www.hipchat.com/docs/apiv2/method/view_room_history
        See http://stackoverflow.com/questions/25226181/how-to-get-all-message-history-from-hipchat-for-a-room-via-the-api
        """
        # Handle recently created rooms...
        if startDate != 'recent' and room['created'] > startDate.replace(tzinfo=room['created'].tzinfo):
            return []

        if room['created'] > endDate.replace(tzinfo=room['created'].tzinfo):
            endDate = room['created']

        params = {
            'date': startDate,
            'max-results': 1000,
            'reverse': reverse,
            'end-date': endDate
        }

        try:
            resp = room._requests.get(room.url + '/history', params=params)
            history = hypchat.Linker._obj_from_text(resp.text, room._requests)
            return history.contents()
        except Exception as e:
            log.warn(e)
            return []

    def older_than(self, date, hours):
        """
        Test whether a date (e.g the date a message was posted) is
        older than a given number of hours.
        """
        # Never-used rooms return Null for last-active...
        if not type(date) == datetime.datetime:
            return True

        # make sure tz is same for comparison or python will complain...
        now = datetime.datetime.now(date.tzinfo)

        interval = now - date

        # Doesn't need to be accurate. Round down to nearest hour is OK
        hours_passed = int(interval.total_seconds()) / 3600

        return hours_passed > hours

    def yesterday(self):
        """
        Returns a datetime instance for yesterday right at the end of
        the day (23h59m59s)
        """
        return self.past_day(1)

    def past_day(self, days_ago):
        """
        Returns a datetime instance some number of days into the past,
        right at the end of the day (23h59m59s)
        """
        day = datetime.datetime.now() - datetime.timedelta(days=days_ago)
        return datetime.datetime(day.year, day.month, day.day, 23, 59, 59)

    def load_emoticons(self):
        """
        Combined a list of standard emoticons with a list of custom
        emoticons retrieved from the Hipchat API
        """
        emoticons = [
            ":)", ":-)", ":(", ":-(", ":-D",
            ":o", ":Z", ":p", ";p", ">:-(",
            "8-)", ":'(", ":(", ":#",
            "(embarrassed)", "O:)",
            ":-*", ":$", "(oops)",
            ":\\", "=)", ":|",
            "(thumbsdown)",
            "(thumbsup)",
            ";-)"
        ]

        emots = self.connection.emoticons()
        for emot in emots['items']:
            emoticons.append('(%s)' % emot['shortcut'])

        re_str, sep = "", ""
        for emoticon in emoticons:
            re_str += sep + re.escape(emoticon)
            sep = "|"
        self.emoticon_re = re.compile(re_str)

    def match_emoticons(self, message):
        """
        Finds all the emoticons in a message and returns them as a list
        """
        if not self.emoticon_re:
            self.load_emoticons()
        return self.emoticon_re.findall(message)
