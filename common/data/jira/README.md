# Atlassian Jira Squirro Data Loader DataSource

This is a plugin for the Squirro Data Loader that allows you to load data directly from any Jira instance.

It requires the v2 Rest API.
See https://docs.atlassian.com/jira/REST/latest/

## Usage

Place the jira.py file on your disk, the use the following argument for squirro_data_load:

```
--source-script path/to/jira.py
```

The datasource plugin will now add these additional arguments:

```
--jira-url https://nektoon.atlassian.net \
--jira-user joe \
--jira-password 'secret!' \
--jira-query 'project != "Nektoon"' \
--jira-batch-size 100 \
```

The jira-query argument takes any valid JQL query.
See: https://confluence.atlassian.com/jirasoftwarecloud/advanced-searching-764478330.html

Note: Don't sort your results, the data loader will do that. Also don't do date restrictions this might break the incremental loading.

## What is missing?

Comments and Audit logs are the two biggest missing features.

Also some good pipelets for e.g. feature extraction etc. would be great.

## Tips

To get documentation on customfields append /rest/api/2/field to the JIRA
URL e.g. https://nektoon.atlassian.net/rest/api/2/field

