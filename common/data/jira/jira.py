"""
Load data from Jira into Squirro
"""
#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Team         : iQuest DWH
# Program      : csv.py
# Description  : Data Source module: csv. Execute full load from a csv file
#


import logging
import os, sys
import csv
import json
import time
import urllib2
import hashlib
import requests
import collections
import dateutil.parser

from squirro.dataloader.data_source import DataSource

VERSION = '0.0.1'
log = logging.getLogger(__name__)


class JiraSource(DataSource):
    """
    Jira Plugin for the sq_data_loader
    """

    def __init__(self):
        self.args = None
        self.skip_fields = ['timespent', 'workratio', 'progress',
                            'aggregateprogress', 'worklog', 'lastViewed']

    def connect(self, inc_column=None, max_inc_value=None):
        """Nothing needed, as jira uses basic auth"""

        log.debug('Incremental Column: %r', inc_column)
        log.debug('Incremental Last Value: %r', max_inc_value)

        #setup requests session
        self.requests = requests.Session()

        #setup base url
        self.base_url = "{0}/rest/api/2/".format(self.args.jira_url)
        log.info('Base URL: %s', self.base_url)

        jql_query = self.args.jira_query

        #is this an incremental load?
        if max_inc_value:
            #parse the date
            max_inc_date = dateutil.parser.parse(max_inc_value)
            jql_date = max_inc_date.strftime('%Y/%m/%d %H:%M')

            #handle incremental loading based on what the query is
            if jql_query == "":
                jql_query = 'updated >= "{0}"'.format(jql_date)
            else:
                query_template = '( {0} ) AND updated >= "{1}"'
                jql_query = query_template.format(jql_query, jql_date)

        #extend the jql query with the order command
        jql_query = '{0} ORDER BY updated asc'.format(jql_query)

        log.info('Running the initial query: %r', jql_query)

        jql_query = urllib2.quote(jql_query, '')

        query_template = 'search?jql={0}&fields=*all&expand=renderedFields'
        self.query = query_template.format(jql_query)

        self.url = "{0}{1}".format(self.base_url, self.query)

        log.info("Sending Request: %r", self.url)

        self.user = self.args.jira_user
        self.pwd = self.args.jira_password

        self.data = None

        #get  the first batch
        self.query_jira(self.url, 0, self.args.jira_batch_size)

        if not self.data:
            raise Exception('No data found')

        log.info('Found %i issues to index', self.data.get('total'))

        log.debug('Sample record: %s', json.dumps(self.data.get('issues')[0], indent=4))

        #prepare the schema from the first row
        sample_data = self.transform_issue(self.data.get('issues')[0])


        self.schema = list(sample_data.keys())

        if sample_data.has_key('Customers'):
            log.info('Sample data: %s', json.dumps(sample_data, indent=4))


    def disconnect(self):
        """Disconnect from the source."""
        #nothing to do
        pass


    def getDataBatch(self, batch_size):
        """
        Generator - Get data from source on batches.
        :returns a list of dictionaries
        """

        while self.data:

            issues = []

            response_issues = self.data.get('issues')

            if not response_issues:
                raise Exception('No data found to load!')

            for issue in response_issues:
                log.debug('Transforming Issue')
                transformed_issue = self.transform_issue(issue)

                if not transformed_issue:
                    log.debug('Skipping invalid issue')
                    continue

                issues.append(transformed_issue)

            yield issues

            #is there more data?
            total = self.data.get('total')
            start_at = self.data.get('startAt')
            max_results = self.data.get('maxResults')

            if start_at + max_results >= total:
                self.data = None
            else:
                self.query_jira(self.url, start_at+max_results, max_results)


    def getJobId(self):
        """
        Return a unique string for each diffrent select
        :returns a string
        """

        #generate a stable id that changes if any of the jira
        #parameters change, this will force a re-index if it changes

        m = hashlib.sha256()
        m.update(self.args.jira_url)
        m.update(self.args.jira_user)
        m.update(self.args.jira_query)
        job_id = m.hexdigest()

        log.info("Job ID: %s", job_id)

        #return str(job_id)
        return job_id


    def getSchema(self):
        """
        Return the schema of the dataset
        :returns a List containing the names of the columns retrieved from the source
        """

        if self.schema:
            log.debug('Schema: %r', self.schema)

            log.info("Columns available for templating and facets:")

            for column in sorted(self.schema):
                log.info("- %s", column)

            return self.schema
        else:
            log.warn('No schema')
            return []


    def add_args(self, parser):
        """
        Add source arguments to the main argumens parser
        """
        source_options = parser.add_argument_group("Source Options")

        source_options.add_argument('--jira-url',
                                    help="Endpoint of the Jira instance")
        source_options.add_argument('--jira-user',
                                    help="Jira username")
        source_options.add_argument('--jira-password',
                                    help='Jira password')
        source_options.add_argument('--jira-query',
                                    help='Jira JQL query, Default: ""')
        source_options.add_argument('--jira-batch-size', default=100, type=int,
                                    help='How many items are fetched per' \
                                         'REST transaction. Default: 100')

        return parser


    def transform_issue(self, issue):
        """
        Transforms a single jira issue rest format
        into a flat list of columns suitable for the data loader
        """

        log.debug("Issue Data before transformation: %s", json.dumps(issue, indent=4))

        columns = {}

        key = issue.get('key')

        if not key:
            raise Exception('Failed to load corrupt item without a key: %r',
                            issue)

        usernames = set()

        columns['id'] = key
        columns['key'] = key
        columns['jira'] = self.args.jira_url
        columns['customers'] = []

        #transform all fields

        for key,value in issue.get('fields', []).iteritems():

            log.debug(key)
            log.debug(type(value))

            if not value and value != 0:
                log.debug('skipped empty %s', key)
                continue  #null and none values get skipped

            if key in self.skip_fields:
                continue

            # Custom Fields can be listed via /rest/api/2/field
            # customfield_11590 - Customer Field - SUP project
            #  -- ignoring for now - it always contains numbers
            # customfield_11390 - Customers Field - All projects
            customfield_map = {
                'customfield_11590':'customers',
                'customfield_11390':'customers'
                }

            if key.find('customfield_') == 0:
                # ignore fields we dont care about
                # see https://nektoon.atlassian.net/rest/api/2/field
                if key not in customfield_map.keys():
                    continue

                # allow multiple fields to map to one column
                if not columns.has_key(customfield_map[key]):
                    columns[key] = []

                # massage all values as lists
                if type(value) == list:
                    columns[customfield_map[key]] += value # customfield_11390
                elif type(value) == dict:
                    columns[customfield_map[key]] += [value['value']] # customfield_11590
                else:
                    columns[customfield_map[key]] += [value] # anything else?


            if key in customfield_map.keys():
                log.debug(
                        "[%s] Found the key %s : value: %s : col: %s" % (
                            columns['id'],
                            key,
                            json.dumps(value, indent=4),
                            json.dumps(columns[customfield_map[key]], indent=4)
                            )
                        )

            #special treatment for the votes field
            if key == 'votes':
                columns['votes'] = value.get('votes', 0)

            #special treatment for the watches field
            if key == 'watches':
                columns['watchers'] = value.get('watchCount', 0)

            #transform the various dicts
            if type(value) == dict:
                log.debug("dict values: %s", json.dumps(value, indent=4))
                #get the id field if available
                id_value = value.get('id')
                if id_value:
                    columns['{0}_{1}'.format(key, 'id')] = id_value

                #get the name field if available
                name_value = value.get('name')
                if name_value:
                    columns['{0}_{1}'.format(key, 'name')] = name_value

                #get the key field if available
                key_value = value.get('key')
                if key_value:
                    columns['{0}_{1}'.format(key, 'key')] = key_value

                #get the displayname field if available
                dn_value = value.get('displayName')
                if dn_value:
                    columns['{0}_{1}'.format(key, 'displayName')] = dn_value

            else:
                columns[key] = value

        #parse the dates
        if 'updated' in columns:
            columns['updated'] = self.get_date(columns['updated'])

        if 'created' in columns:
            columns['created'] = self.get_date(columns['created'])

        #ensure title and body are always there, even if empty
        if not 'summary' in columns:
            columns['summary'] = ""

        if not 'description' in columns:
            columns['description'] = ""

        #get the html rendered body
        description_html = issue['renderedFields'].get('description')

        if not description_html:
            columns['description_html'] = ""
        else:
            columns['description_html'] = description_html

        #get the plaintext comments, and serialize them
        comments = issue['fields']['comment']['comments']

        log.debug("Found %i comments", len(comments))

        comment_strs = []
        for comment in comments:

            comment_data = {}
            comment_data['body'] = comment.get('body', '')
            comment_data['author'] = comment['author']['displayName']
            comment_data['created'] = comment['created']

            usernames.add(comment['author']['displayName'])

            comment_strs.append(json.dumps(comment_data))

        columns['comments'] = '<--|-->'.join(comment_strs)


        #get the html rendered comments, and serialize them
        comments = issue['renderedFields']['comment']['comments']

        log.debug("Found %i comments", len(comments))

        comment_strs = []
        for comment in comments:

            comment_data = {}
            comment_data['body'] = comment.get('body', '')
            comment_data['author'] = comment['author']['displayName']
            comment_data['created'] = comment['created']

            usernames.add(comment['author']['displayName'])

            comment_strs.append(json.dumps(comment_data))

        columns['comments_html'] = '<--|-->'.join(comment_strs)

        #set the link
        columns['url'] = '{0}/browse/{1}'.format(self.args.jira_url,
                                                 columns['key'])

        #if the assignee is not set, set it to "Unassigned"
        if not 'assignee_displayName' in columns:
            columns['assignee_displayName'] = 'Unassigned'
        else:
            usernames.add(columns['assignee_displayName'])

        log.debug('Date: %s, %r', type(columns['updated']), columns['updated'])

        #be nice to endusers
        columns = collections.OrderedDict(sorted(columns.items()))

        log.debug("Issue Data after transformation: %s", json.dumps(columns,
                                                                    indent=4))

        usernames.add(columns['creator_displayName'])
        usernames.add(columns['reporter_displayName'])

        columns['names'] = ";".join(usernames)
        columns['customers'] = ";".join(columns['customers'])

        return columns


    def get_date(self, date_string):
        """Turns ISO-8601 et all into a python datetime object"""

        return dateutil.parser.parse(date_string).strftime('%Y-%m-%dT%H:%M:%S')


    def query_jira(self, url, start=0, batch_size=10):
        """Query data from Jira, store it into self.data"""

        full_url = "{0}&maxResults={1}&startAt={2}".format(url, batch_size,
                                                           start)

        self.result = self.requests.get(full_url, auth=(self.user, self.pwd))

        if self.result.status_code != 200:
            log.error('Error %i returned by jira api: %r',
                      self.result.status_code, self.result.text)

            self.data = None
        else:
            self.data = self.result.json()
