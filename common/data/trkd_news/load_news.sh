#! /bin/bash

source ../../config/config.sh

squirro_data_load -vvv \
--cluster $TESTING_CLUSTER \
--token $TESTING_TOKEN \
--project-id $TESTING_PROJECT  \
--source-script 'trkd_plugin.py' \
--source-batch-size 50 \
--trkd-username  'trkd-demo-wm@thomsonreuters.com' \
--trkd-password  'u3v4p95cl' \
--trkd-appid  'trkddemoappwm' \
--trkd-timestamp-file  'timestamp.json' \
--trkd-max-articles  '500' \
--map-title 'title' \
--map-body 'body' \
--pipelets-file 'pipelets_config.json' \
--source-name 'TRKD News' \
--facets-file facets.json
