"""
Load News from TRKD into Squirro
"""
import hashlib
import logging
import ujson as json
from bs4 import BeautifulSoup
import requests
from datetime import datetime

from squirro.dataloader.data_source import DataSource

VERSION = '0.0.1'
log = logging.getLogger(__name__)


class TRKDSource(DataSource):
    """
    TRKD News Plugin for the sq_data_loader
    """

    def __init__(self):
        self.args = None

    def connect(self, inc_column=None, max_inc_value=None):
        log.debug('Incremental Column: %r', inc_column)
        log.debug('Incremental Last Value: %r', max_inc_value)


    def disconnect(self):
        """Disconnect from the source."""
        # Nothing to do
        pass

    def setup_class_vars(self):
        self.max_articles = int(self.args.trkd_max_articles)
        self.tr_username = self.args.trkd_username
        self.tr_password = self.args.trkd_password
        self.tr_appid = self.args.trkd_appid
        self.tr_timestamp_file = self.args.trkd_timestamp_file

        try:
            with open(self.tr_timestamp_file, 'r') as timestamp_file:
                self.timestamp_list = json.load(timestamp_file)

            self.tr_latest_timestamp = self.timestamp_list[-1]

        except IOError:
            # handling for missing timestamp file
            self.timestamp_list = []
            self.tr_latest_timestamp = '2001-01-01T00:00:00'

    def getDataBatch(self, batch_size):
        """
        Generator - Get data from source on batches.
        :returns a list of dictionaries
        """

        self.setup_class_vars()

        articles_to_upload = self.get_new_articles()

        if len(articles_to_upload) == 0:
            log.info('There are no new articles at this time')

        else:
            log.info('Got {} new articles from TRKD'.format(len(articles_to_upload)))
            squirro_items = []

            for article in articles_to_upload:
                if isinstance(article.get('Text'), basestring):
                    squirro_items.append(
                        {
                            "title": article['Headline'],
                            "body": article['Text'] ,
                            "created_at": article['Time'][:19],
                            "id": hashlib.md5(article['Story_ID']).hexdigest(),
                            "external_id": hashlib.md5(article['Story_ID']).hexdigest()
                        }
                    )

                    if len(squirro_items) >= batch_size:
                        yield squirro_items
                        squirro_items = []

            if squirro_items:
                yield squirro_items


    def getSchema(self):
        """
        Return the schema of the dataset
        :returns a List containing the names of the columns retrieved from the
        source
        """

        return ['title', 'body', 'created_at', 'id', 'story_id']


    def getJobId(self):
        """
        Return a unique string for each different select
        :returns a string
        """
        # Generate a stable id that changes if any of the main parameters
        self.setup_class_vars()

        m = hashlib.sha256()
        m.update(self.tr_latest_timestamp)
        m.update(self.tr_appid)
        job_id = m.hexdigest()
        log.debug("Job ID: %s", job_id)
        #return job_id
        return job_id


    def add_args(self, parser):
        """
        Add source arguments to the main arguments parser
        """
        source_options = parser.add_argument_group('Source Options')

        source_options.add_argument('--trkd-username',
                                    help="TRKD Username")
        source_options.add_argument('--trkd-password',
                                    help='TRKD Password')
        source_options.add_argument('--trkd-appid',
                                    help='TRKD Application ID')
        source_options.add_argument('--trkd-timestamp-file',
                                    help='Timestamp file of most recent loads from TRKD')
        source_options.add_argument('--trkd-max-articles',
                                    help='Maximum number of articles to get from TRKD')

        return parser


    # The News Loader
    def get_new_token(self):

        body = {
            "ApplicationID": self.tr_appid,
            "Username": self.tr_username,
            "Password": self.tr_password
        }

        xml = '''<Envelope xmlns="http://www.w3.org/2003/05/soap-envelope">
        <Header>
        <To xmlns="http://www.w3.org/2005/08/addressing">https://api.rkd.reuters.com/api/TokenManagement/TokenManagement.svc/Anonymous</To>
        <MessageID xmlns="http://www.w3.org/2005/08/addressing">[Unique Message ID]</MessageID>
        <Action xmlns="http://www.w3.org/2005/08/addressing">http://www.reuters.com/ns/2006/05/01/webservices/rkd/TokenManagement_1/CreateServiceToken_1</Action>
        </Header>
        <Body >
        <CreateServiceToken_Request_1 xmlns:global="http://www.reuters.com/ns/2006/05/01/webservices/rkd/Common_1" xmlns="http://www.reuters.com/ns/2006/05/01/webservices/rkd/TokenManagement_1">
        <global:ApplicationID>{}</global:ApplicationID>
        <Username>{}</Username>
        <Password>{}</Password>
        </CreateServiceToken_Request_1>
        </Body>
        </Envelope>'''.format(self.tr_appid, self.tr_username, self.tr_password)

        headers = {
            "Content-Type": "application/soap+xml",
            "Action": "http://www.reuters.com/ns/2006/05/01/webservices/rkd/TokenManagement_1/CreateServiceToken_1"
        }

        tr_response = requests.post('https://api.rkd.reuters.com/api/TokenManagement/TokenManagement.svc/Anonymous', headers=headers, data=xml)

        bs_response = BeautifulSoup(tr_response.text, 'lxml')
        new_token = bs_response.find("global:token").text
        return new_token


    def get_headlines(self):
        tr_token = self.get_new_token()
        api_url = 'http://api.rkd.reuters.com/api/News/News.svc/REST/News_1/RetrieveHeadlineML_1'

        headline_headers  = {
            "Content-Type": "application/json; charset=utf-8",
            "X-Trkd-Auth-Token": tr_token,
            "X-Trkd-Auth-ApplicationID": self.tr_appid
        }

        headline_body = {
                          "RetrieveHeadlineML_Request_1": {
                            "HeadlineMLRequest": {
                              "MaxCount": self.max_articles,
                              "StartTime": self.tr_latest_timestamp,
                              "Filter": [
                                {
                                  "And": {
                                    "MetaDataConstraint_typehint": [
                                      "MetaDataConstraint",
                                      "MetaDataConstraint"
                                    ],
                                    "MetaDataConstraint": [
                                      {
                                        "class": "topics",
                                        "Value": {
                                          "Text": "US"
                                        }
                                      },
                                      {
                                        "class": "language",
                                        "Value": {
                                          "Text": "en"
                                        }
                                      }
                                    ]
                                  }
                                }
                              ]
                            }
                          }
                        }

        try:
            headline_response = requests.post(api_url, headers=headline_headers, data=json.dumps(headline_body))
            json_headlines = json.loads(headline_response.text)
            headline_list = json_headlines['RetrieveHeadlineML_Response_1']['HeadlineMLResponse']['HEADLINEML']['HL']
            simple_headline_list = [{"ID": headline['ID'], "Text": headline['HT'], "Time": headline['LT']} for headline in headline_list]
            return simple_headline_list
        except TypeError:
            return []


    def break_into_batches(self, id_list):
        # Breaks a large list of IDs into a nested list with batches of 30

        list_length = len(id_list)

        if list_length <= 30:
            return [id_list]

        else:
            nested_list = []

            for i in range(0, list_length // 30):
                nested_list.append(id_list[(30 * i):(30 * (i + 1))])

            remaining_elements = list_length % 30
            nested_list.append(id_list[-1 * (remaining_elements):])

            return nested_list


    def get_full_articles(self, headline_list):

        news_ids = [headline['ID'] for headline in headline_list]
        id_batches = self.break_into_batches(news_ids)

        tr_token = self.get_new_token()
        api_url = 'http://api.rkd.reuters.com/api/News/News.svc/REST/News_1/RetrieveStoryML_1'

        story_headers  = {
            "Content-Type": "application/json; charset=utf-8",
            "X-Trkd-Auth-Token": tr_token,
            "X-Trkd-Auth-ApplicationID": self.tr_appid
        }

        full_stories = []

        for batch in id_batches:

            story_body = {
                "RetrieveStoryML_Request_1": {
                    "StoryMLRequest": {
                        "StoryId": batch
                    }
                }
            }

            try:
                story_response = requests.post(api_url, headers=story_headers, data=json.dumps(story_body))
                json_stories = json.loads(story_response.text)
                story_list = json_stories['RetrieveStoryML_Response_1']['StoryMLResponse']['STORYML']['HL']
                clean_story_list = [{"Time": story['LT'], "Text": story['TE'], 'Headline': story['HT'], 'Story_ID': story['ID']} for story in story_list]
                full_stories = full_stories + clean_story_list
            except KeyError:
                continue

        return full_stories


    def get_new_articles(self):
        # Gets the newest articles, up to the argument max_number
        # Only gets articles that are newer than the saved timestamp in

        articles = self.get_full_articles(self.get_headlines())

        # Early exit if there are no new articles
        if len(articles) == 0:
            return articles

        timestamp_list = [int(datetime.strftime(datetime.strptime(article['Time'][:19], '%Y-%m-%dT%H:%M:%S'), '%Y%m%d%H%M%S')) for article in articles]
        timestamp_list.sort()
        new_timestamp = timestamp_list[-1]
        new_ts_string = datetime.strftime(datetime.strptime(str(new_timestamp), '%Y%m%d%H%M%S'), '%Y-%m-%dT%H:%M:%S')

        self.timestamp_list.append(new_ts_string)

        # Keep only the 100 most recent timestamps
        if len(self.timestamp_list) > 100:
            self.timestamp_list = self.timestamp_list[-100:]

        with open(self.tr_timestamp_file, 'w') as timestamp_file:
            json.dump(self.timestamp_list, timestamp_file)

        return articles

