# TRKD News Loader (Squirro Dataloader Plugin)

This is a custom data loading script that loads news articles from the Thomson Reuters TRKD API into squirro.
### Article Source
As provided, the articles are retreived based on two parameters:
```json
{
    "topic": "US",
    "language": "English"
}
```
This gets all english language articles that mention the US, US events, politics, or US companies. This can be changed in the `trkd_plugin.py` file if needed

### Usage
To use the loader, point `load_news.sh` to a config file with the needed values. To load news, run the script `load_news.sh`.

### Incremental Loading
The loader maintains a `timestamp.json` file (in this directory) when run. This file keeps track of the 100 most recent timestamps of the news loading operations.

To continuously load news data, run the following command (in a screen session):
```sh
while true; do ./load_news.sh; sleep 300; done
```
