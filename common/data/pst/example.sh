#!/bin/bash

CLUSTER=...
PROJECT_ID=...
TOKEN=...
SOURCE_PATH=TESTPST.pst

squirro_data_load -vv \
    --cluster $CLUSTER \
    --project-id $PROJECT_ID \
    --token $TOKEN \
    --source-script email_loader.py \
    --source-path $SOURCE_PATH \
    --map-id message-id \
    --map-title subject \
    --map-body body \
    --map-created-at created_at \
    --facets-file facets.json
