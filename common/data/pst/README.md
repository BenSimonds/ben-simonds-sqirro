Plugin to load emails with the data loader.

Currently supports:

- Folder structure with individual eml files
- Outlook PST files


Installation
============

`readpst` needs to exist in the PATH.

For CentOS use the following:

    $ sudo yum install http://www.five-ten-sg.com/libpst/packages/centos6/libpst-libs-0.6.66-1.el6.x86_64.rpm http://www.five-ten-sg.com/libpst/packages/centos6/libpst-0.6.66-1.el6.x86_64.rpm

On Mac use Homebrew:

    $ brew install libpst

