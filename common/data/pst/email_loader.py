# -*- coding: utf-8 -*-
"""Data source implementation for emails on file system.

Usage:

    Two arguments are accepted for this loader.

    --source-path - file or folder to import. If this is a `.pst` file it is
      extracted and all mails are imported. Otherwise it is assumed to point to
      standard mbox files. Can be specified multiple times.
    --output-type - whether to output the emails or the attachments. Valid
      values are `mails` - which is the default - and `attachments`. To import
      both, the loader needs to be executed twice.

    See example.sh for how to use this in anger.

Rows have the following keys:

    - type: Email or Attachment (see --output-type below)
    - body
    - mime_type: The MIME type of the body (for type Email) or the attachment
      (for type Attachment)
    - subject
    - created_at
    - recipients: all recipient email addresses (To, Cc) - separated by |
    - recipient_names: all recipient names (To, Cc) - separated by |
    - sender: sender email address - separated by |
    - sender_name: sender name - separated by |
    - Additionally each message header is made available as well with the
      lower-case name of the header as the key

Attachments also have the following keys:

    - id: An ID that combines both the message_id and the attachment_id into a
      unique identifier
    - filename
    - data: The actual data of the attachment
"""
import datetime
import email
import email.header
import email.utils
import hashlib
import logging
import os
import shutil
import subprocess
import tempfile
import uuid

import pytz
from squirro.dataloader.data_source import DataSource

log = logging.getLogger(__name__)

# Keys that can be returned from an email - this only lists the hard-coded
# ones. In addition to these any header is returned as well.
KEYS = frozenset([
    'subject', 'body', 'mime_type', 'created_at', 'from', 'to', 'message-id',
    'recipients', 'recipient_names', 'sender', 'sender_name',
])

ATTACHMENT_KEYS = frozenset([
    'id', 'filename', 'data',
])

# MIME types for which we import attachments. This still has to be extended a
# bit.
MIME_TYPES = [
    'application/pdf', 'text/pdf', 'text/html',
]


class EmailSource(DataSource):
    def __init__(self):
        self.args = None

    def connect(self, inc_column=None, max_inc_value=None):
        """Create connection with the source."""
        source_paths = self.args.source_paths
        data = []

        for path in source_paths:
            if not os.path.exists(path):
                raise IOError("File or folder {} does not exist".format(path))
            elif os.path.isdir(path):
                data.append({
                    'path': path,
                    'type': 'folder',
                })
            else:
                _, ext = os.path.splitext(path)
                if ext.lower() == '.pst':
                    # Outlook file, extract into temporary folder
                    data.append({
                        'path': self._extract_pst(path),
                        'type': 'pst',
                    })
                else:
                    data.append({
                        'path': path,
                        'type': 'file',
                    })

        self.data = data

    def disconnect(self):
        """Disconnect from the source."""
        for data in self.data:
            if data['type'] == 'pst':
                shutil.rmtree(data['path'])

    def getDataBatch(self, batch_size):
        """
        Generator - Get data from source on batches.

        :returns a list of dictionaries
        """
        items = []

        for data in self.data:
            if data['type'] == 'file':
                for item in self._parse_file(data['path']):
                    items.append(item)
                    if len(items) >= batch_size:
                        yield items
                        items = []
            else:
                for root, dirs, files in os.walk(data['path']):
                    for fname in files:
                        for item in self._parse_file(
                                os.path.join(root, fname)):
                            items.append(item)
                            if len(items) >= batch_size:
                                yield items
                                items = []

        if items:
            yield items

    def _extract_pst(self, fname):
        # Check if `readpst` exists
        try:
            proc = subprocess.Popen(['readpst', '-V'], stdout=subprocess.PIPE)
            _, _ = proc.communicate()
            assert proc.returncode == 0
        except Exception as e:
            errormsg = 'readpst can not be executed. Is it installed?'
            log.error(e)
            raise Exception(errormsg)

        out_path = tempfile.mkdtemp()

        # Extract the PST file
        log.debug('Extracting %s into %s', fname, out_path)
        proc = subprocess.Popen(['readpst', '-re', '-o', out_path, fname],
                                stdout=subprocess.PIPE)
        _, _ = proc.communicate()
        if proc.returncode != 0:
            raise Exception('Could not extract PST file.')

        return out_path

    def _parse_file(self, path):
        with open(path, 'rb') as f:
            obj = email.message_from_file(f)

        if not obj:
            log.warn('Could not parse email: %r', path)

        assert obj.is_multipart()

        # Take the first text/plain payload
        payload = None

        item = None
        attachments = []

        payloads = obj.get_payload()
        for idx, payload in enumerate(payloads):
            if payload.get_content_type() == 'text/plain' and \
                    not payload.get('Content-Disposition'):
                items = [self._parse_payload(payload, obj)]
            elif payload.get('Content-Disposition', '').\
                    startswith('attachment;') and \
                    payload.get_content_type() in MIME_TYPES:
                attachments.append({
                    'filename': payload.get_filename(),
                    'mime_type': payload.get_content_type(),
                    'data': payload.get_payload(decode=True),
                })

            # Handling for nested messages
            elif payload.get_content_type() == 'multipart/alternative' and not payload.get('Content-Disposition'):
                all_messages = payload.get_payload()
                items = []
                for single_message in all_messages:
                    items.append(self._parse_payload(single_message, obj))

            else:
                log.debug('Ignoring message part %d (%s)', idx,
                          payload.get_content_type())
        for item in items:
            if attachments:
                for idx, attachment in enumerate(attachments):
                    msg_id = item.get('message-id') or str(uuid.uuid4())
                    attachment_id = attachment.get('filename') or str(idx)
                    digest = hashlib.sha1()
                    digest.update(msg_id.encode('utf8'))
                    digest.update(attachment_id.encode('utf8'))
                    attachment['id'] = digest.hexdigest()

            if self.args.output_type == 'attachments' and attachments:
                if not item:
                    # Get a basic item without body, so that we have all the
                    # meta-data
                    item = self._parse_payload(None, obj)

                for idx, attachment in enumerate(attachments):
                    out = dict(item)
                    out.update(attachment)
                    out['type'] = 'Attachment'
                    yield out

            if item and self.args.output_type == 'mails':
                item['attachments'] = attachments
                yield item

    def _parse_payload(self, part, msg):

        # Properly handle multiple messages in a chain

        #log.debug('Parsing Payload: {}'.format(part.as_string()))


        # Get the UTC datetime for the date
        created_at_tuple = email.utils.parsedate_tz(msg.get('Date'))
        created_at = datetime.datetime(*created_at_tuple[0:6])
        if created_at_tuple[9]:
            tz_offset = pytz.FixedOffset(created_at_tuple[9] / 60.0)
            created_at = tz_offset.localize(created_at)
            created_at = created_at.astimezone(pytz.utc)

        item = {
            'created_at': created_at,
        }

        if part:
            #body = part.get_payload()
            body = part.as_string()
            log.debug('Got the Body: {}'.format(body))
            body_charset = part.get_param('charset')
            if body_charset:
                try:
                    body = body.decode(body_charset)
                except UnicodeError:
                    log.warn('Invalid body charset %r', body_charset)
            item['body'] = body
            item['mime_type'] = part.get_content_type()

        tos = msg.get_all('to', [])
        ccs = msg.get_all('cc', [])
        resent_tos = msg.get_all('resent-to', [])
        resent_ccs = msg.get_all('resent-cc', [])
        all_recipients = email.utils.getaddresses(
            tos + ccs + resent_tos + resent_ccs)
        item['recipients'] = '|'.join(
            filter(None, [r[1] for r in all_recipients]))
        item['recipient_names'] = '|'.join(
            filter(None, [r[0] for r in all_recipients]))

        sender = None
        sender_name = None
        if msg.get('From'):
            sender, sender_name = email.utils.parseaddr(msg.get('From'))
        item['sender'] = sender
        item['sender_name'] = sender_name

        for key, value in msg.items():
            key = key.lower()
            if key not in item:
                for (decoded, charset) in email.header.decode_header(value):
                    if charset:
                        try:
                            decoded = decoded.decode(charset)
                        except UnicodeError:
                            log.warn('Invalid header value %r for charset %r',
                                     decoded, charset)
                    # TODO: Only the last value is used at the moment
                    item[key] = decoded

        return item

    def getJobId(self):
        """
        Return a unique string for each different select

        :returns a string
        """
        paths = [os.getcwd()]
        paths.extend(self.args.source_paths or [])
        return ','.join(sorted(paths))

        return os.path.basename(self.args.source_path)

    def getSchema(self):
        """
        Return the schema of the data set

        :returns a List containing the names of the columns retrieved from the
            source
        """
        ret = KEYS
        if self.args.output_type == 'attachments':
            ret = ret | ATTACHMENT_KEYS
        return ret

    def add_args(self, parser):
        """
        Add source arguments to the main arguments parser
        """
        source_options = parser.add_argument_group("Source Options")
        source_options.add_argument(
            '--source-path', action='append', dest='source_paths',
            required=True,
            help='Paths of email folder or files. Can be specified multiple '
                 'times.')
        source_options.add_argument(
            '--output-type', default='mails', choices=['mails', 'attachments'],
            help='Whether to emit the emails or the attachments. '
                 'Default: %(default)s.')

        return parser
