# Helper script that handles config and environment handling for our projects.
# Use it as follows:
#   source $(dirname $0)/../common/config/config.sh
#
# This will read the main.ini file from the project's config file. For this
# it's assumed, that the `common` folder is on the same level as the project's
# `config` folder.

PROJECT_ROOT="$(cd $(dirname "${BASH_SOURCE[0]}")/../.. && pwd)"

getval () {
    # Retrieve a configuration value from the ini file.
    #
    # Parameters:
    #    - $1: key
    #    - $2 (optional): section - defaults to `squirro`
    key=$1
    section=squirro
    if [ -n "$2" ]; then
        section="$2"
    fi
    file=$PROJECT_ROOT/config/main.ini
    ret=$(awk "/\[${section}\]/{p=1;print;next} p&&/^\[/{p=0};p" $file | \
        grep "^ *${key} *=" | sed 's/[^=]*= *\(.*\) *$/\1/')
    if [ -z "$ret" ]; then
        echo "Did not find value for $section.$key" >&2
        return 1
    fi
    echo $ret | sed "s#%(root)s#$PROJECT_ROOT#g"
}

if [ -f /opt/rh/python27/enable ]; then
    . /opt/rh/python27/enable
    . /opt/squirro/virtualenv/bin/activate
elif [ -f ~/.virtualenvs/squirro/bin/activate ]; then
    source ~/.virtualenvs/squirro/bin/activate
fi

CLUSTER=$(getval cluster)
TOKEN=$(getval token)
PROJECT_ID=$(getval project_id)
