"""
This pipelet identifies catalysts using the Thomson Reuters Intelligent Tagging API
"""

from squirro.sdk import PipeletV1, require
from hashlib import sha256
import json
import os
import time
import os.path
import errno
import requests
from datetime import datetime

CALAIS_API_URL = "https://api.thomsonreuters.com/permid/calais"

@require('requests')
@require('log')
class OpenCalaisPipelet(PipeletV1):

    def __init__(self, config):
        self.config = config


    def consume(self, item):

        self._enrich(item)

        item['keywords']['tr_catalysts'] = ['1']

        return item


    def _enrich(self, item):

        keywords = item.setdefault('keywords', {})

        if not self.config.get('api_key'):
            self.log.error('No opencalias api key present, please set api_key in the pipelet config.')
            return

        #prep the item
        if 'body' in item:
            body = item['body']

        elif 'sub_items' in item:
            body = ''

            for sub_item in item.get('sub_items'):

                if len(body) > 180 * 1024:
                    break

                body += sub_item.get('body', '')

            if body == '':
               return

        else:
            return

        headers = {
            'Content-Type': 'text/html; charset=utf-8',
            'outputFormat': 'application/json',
            'x-calais-language': 'English',
            'x-ag-access-token': self.config.get('api_key')
            }

        data=body.encode('utf-8')

        # Delay to avoid going over the API Key usage limit
        time.sleep(2)

        res_data = self.get_response(
            request=CALAIS_API_URL,
            cache_location='/tmp/response_cache/',
            timeout=None,
            http='POST',
            headers=headers,
            data=data
            )

        res_data = res_data['data']

        if not isinstance(res_data, dict):
            self.log.error('Got the bad API response: {}'.format(repr(res_data)))
            item['keywords']['bad_response'] = ['True']
            return item

        # Swap out the body for the clean one used by open calais
        item['body'] = res_data['doc']['info']['document']

        # Loop over every item in the API response, and add a tag for it
        for key, value in res_data.iteritems():

            # Processs Relations
            if value['_typeGroup'] == 'relations':
                self.process_relation(value, res_data, item)

        if not item['keywords'].get('Catalyst'):
            self.tag_with_text('Catalyst', 'False', item)

        return item


    def process_relation(self, value, item):

        event_type = value['_type']

        if event_type in [
            'Acquisition',
            'EmploymentChange',
            'AnalystEarningsEstimate',
            'CompanyEarningsAnnouncement',
            'CompanyEarningsGuidance',
            'AnalystRecommendation',
            'SecondaryIssuance',
            'BonusSharesIssuance',
            'EquityFinancing',
            'DebtFinancing',
            'Deal',
            'CompanyRestatement',
            'CompanyReorganization',
            'CreditRating',
            'PatentFiling',
            'PatentIssuance',
            'IPO',
            'CompanyLegalIssues',
            'Dividend',
            'Buybacks',
            'Merger',
            'Bankruptcy',
            'CompanyAccountingChange',
            'CompanyInvestigation',
            'CompanyInvestment',
            'CompanyLayoffs']:

            self.tag_with_text('Catalyst_Type', value['_type'], item)
            self.tag_with_text('Catalyst', 'True', item)

        catalyst_groups = {
            "Earnings": [
                'AnalystEarningEstimate',
                'CompanyEarningAnnouncement',
                'CompanyEarningGuidance',
                'Dividend',
                'ConferenceCall',
                'CompanyMeeting'],
            "New_Issues": [
                'SecondaryIssuance',
                'IPO',
                'EquityFinancing',
                'BonusSharesIssuance'],
            "Drew_Down_on_Revolver": [
                'DebtFinancing',
                'BuyBack',
                'Bankruptcy',
                'EquityFinancing',
                'Dividend',
                'CompanyInvestment'],
            "M&A": [
                'Acquisition',
                'Merger',
                'Deal'],
            "Forecast_Change": [
                'CompanyRestatement',
                'AnalystEarningEstimate',
                'CompanyEarningAnnouncement',
                'CompanyEarningGuidance',
                'AnalystRecommendation',
                'CompanyAccountingChange'],
            "Top_Management_Change": [
                'CompanyReorganization',
                'CompanyLayoffs',
                'EmploymentChange'],
            "Rating_Agency_Changes": [
                'CreditRating'],
            "IP_Changes": [
                'CompanyLegalIssues',
                'PatentFiling',
                'PatentIssuance'],
            "Regulation": [
                'CompanyInvestigation',
                'FDAPhase']
        }

        for group_name, group_events in catalyst_groups.iteritems():
            if event_type in group_events:
                self.tag_with_text('Catalyst_Group', group_name, item)


    def tag_with_text(self, facet_name, text, item):

        if isinstance(text, list):

            for individual_text in text:
                self.tag_with_text(facet_name, individual_text, item)

        else:
            item['keywords'].setdefault(facet_name, [])

            if (text) and (text not in item['keywords'][facet_name]) :
                item['keywords'][facet_name].append(text)


    # API Response caching framework
    # The main method, This is what is called to get a remote cached API response
    def get_response(self, request, cache_location='/tmp/response_cache/', store_errors=False, timeout=None, http='GET', headers=None, data=None):

        # Check the cache first, then if no cache, go to the API to get the folder data
        response_data = self._lookup_cache(request, headers, data, cache_location, timeout)

        # Cache Miss
        if not response_data:
            # Make API Request
            response = self.get_web_response(request, http, headers, data)

            if (store_errors) or (int(response['status_code']) < 300):

                try:
                    # Write to cache after successful API response
                    self._write_cache(request, headers, data, response, cache_location)

                except ValueError:

                    # If writing the first time fails, it is useful to try
                    # a second time with the encoding set
                    try:
                        self._write_cache(request, headers, data, response.encode('utf8'), cache_location)

                    except UnicodeEncodeError:
                        print 'Error writing the response to the cache'

        # Cache Hit
        else:
            # work with the res_data as if it were the API response
            response = response_data

        return response


    def get_web_response(self, request, http, headers, data):

        if http in ['GET', 'Get', 'get']:
            response = requests.get(request, headers=headers, data=data)

        elif http in ['POST', 'Post', 'post']:
            response =  requests.post(request, headers=headers, data=data)

        else:
            return None


        try:
            response_data = json.loads(response.text)

        except Exception:
            response_data = response.text

        response_status = response.status_code

        response_dict = {
            "data": response_data,
            "status_code": response_status
        }

        return response_dict


    def _lookup_cache(self, request, headers, data, cache_location, timeout):

        file_path = self._cache_file_name(request, headers, data, cache_location)
        path = os.path.dirname(file_path)

        try:
            os.makedirs(path)

        except OSError as ex:
            if ex.errno != errno.EEXIST:
                print 'Could not create cache folder %s', path
                return None

        try:

            # Try to get the last modified date of the file
            if (self.get_cache_age(file_path) < timeout) or (not timeout):
                with open(file_path, 'rb') as f:

                    response = json.load(f)['response']
                    return response

            else:
                return None

        except Exception:
            return None


    def get_cache_age(self, file_path):

        modified_time = self.get_modified_time(file_path)
        current_time = datetime.now()

        cache_age = current_time - modified_time
        age_days = cache_age.days
        age_seconds = cache_age.seconds
        # Calculate the total number of second elapsed
        total_age_seconds = age_seconds + (86400 * age_days)

        # Convert the total age in seconds into hours (rounded down)
        total_age_hours = total_age_seconds // 3600

        return total_age_hours



    def get_modified_time(self, file_path):

        time_stamp = os.path.getmtime(file_path)

        return datetime.fromtimestamp(time_stamp)


    def _write_cache(self, request, headers, data, response, cache_location):

        file_path = self._cache_file_name(request, headers, data, cache_location)
        json_object = {'cache-key': request, 'response': response}

        with open(file_path, 'wb') as f:
            json.dump(json_object, f)


    def _cache_file_name(self, request, headers, data, cache_location):

        digest_body = str({
            "request": repr(request),
            "headers": headers,
            "data": data
            })
        digest = sha256(digest_body).hexdigest()

        path = os.path.join(cache_location, digest[:2], digest[2:4], digest[4:6])
        file_path = path + '/' + digest + '.json'

        return file_path

