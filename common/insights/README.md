# Catalyst Detection

## Open Calais Catalyst Detection
This pipelet uses the Thomson Reuters Open Calais / Intelligent Tagging API to identify events which are mentioned within an article. Using the events mentioned, the pipelet determines whether or not the document can be a catalyst, and if so adds the type of catalyst.

## Known Issues
Catalyst status, as of now, is not linked to any specific entity. If a document mentions an acquisition of 'Company A' by 'Company B', the document will always be considered a catalyst, even within the context of a third Company C.
