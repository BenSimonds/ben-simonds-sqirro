# Squirro Install Notes

Bit of a pain getting the squirro vm to access the internet on my mac. Here's what worked.

1. Set up the vm with one network interface, a bridged adapter. 

2. Set a specific ipv4 ip once logged into the host machine.

```

sudo ipconfig eth0 192.168.0.55 netmask 255.255.255.0 arp
sudo route add default gw 192.168.0.1

```

This page was actually useful:

http://apple.stackexchange.com/questions/201183/how-to-bridge-vboxnet-with-wi-fi-on-os-x-10-10-yosemite

From there you should be able to log in via ssh.

```
ssh squirro@192.168.0.55

```

Add a user for the sake of not using the squirro account all the time.

```
sudo -s
useradd ben
passwd ben

```

Also add it to the sudoers file. You might have to log in again for this to take effect.

```
usermod -aG wheel ben

```

It's also nice to copy across your ssh key so you can ssh in without entering passwords like a mug. From the host machine (not the remote) use ssh-copy-id (install it with homebrew if not present).

```
ssh-copy-id ben@192.168.0.55 

```

## Squirro Endpoint Setup
On your first login, sign up as a new user.


## Project Setup

* Open up the server in a browser and create a new project.

* When starting from scratch you'll want to clone the delivery repository from Squirro's github. This will also require adding the ssh key on this machine to your github account. Once that's done you can init the common submodule too.

```
git submodule init
git submodule update
``` 

* Next we need to grab our various token settings and update our main.ini file (config/main.ini).

* Apply settings using config.sh

```
 source common/config/config.sh

```


## Troubleshooting

### nginx wont start 

Probably SELinux related problems if you've copied stuff over. Take a look with `ls -Z /etc/nginx/` and try restoring permissions with `restorecon -r /etc/nginx/` then restart nginx with `service nginx restart` (all of the above as root btw).

Useful links for this: 
 * https://stackoverflow.com/questions/6795350/nginx-403-forbidden-for-all-files
 * https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Security-Enhanced_Linux/sect-Security-Enhanced_Linux-SELinux_Contexts_Labeling_Files-Persistent_Changes_semanage_fcontext.html

## Cant get anything of the squirro github repositories

Have you added your ssh key to your github account. Have you generated an rsa key?

##