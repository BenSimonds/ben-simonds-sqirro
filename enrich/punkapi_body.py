from squirro.sdk import PipeletV1
#from bs4 import BeautifulSoup as bs

class PunkAPIBody(PipeletV1):
    def consume(self, item):
    	title = item.get('title')
    	body = item.get('body')
        keywords = item.get('keywords', {})
        
        new_body = '<h2> {0} </h2> <p> {1} </p> <img src= "{2}"></img>'.format(title, body, str(keywords['img_url'][0]))

        item['body'] = new_body


        return item