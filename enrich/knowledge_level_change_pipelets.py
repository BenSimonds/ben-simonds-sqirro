from squirro.sdk import PipeletV1

class KnowledgeLevelChange(PipeletV1):
    def consume(self, item):
        keywords = item.get('keywords', {})
        pre = keywords.get('Pre-webinar knowledge level', [])
        post = keywords.get('Post-webinar knowledge level', [])
        if len(pre) == 1 and len(post) == 1:
            change = post[0]-pre[0]
            print('KLC', change)
            item['keywords']['Change in Knowledge Level'] = [change]

        return item