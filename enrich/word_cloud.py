from squirro.sdk import PipeletV1
import re
from nltk.corpus import stopwords

class WordCloud(PipeletV1):
    def consume(self, item):
        our_stopwords = ['i', 'webinar', 'webinars', 'also', 'the', 'would', 'it']
        body = item.get('body').encode('utf-8')
        word_cloud = re.compile('\w+').findall(body)
        terms = [word.lower() for word in word_cloud if word not in stopwords.words('english')]
        terms = [term for term in terms if term not in our_stopwords]
        # terms = list(set(terms))

        item['keywords']['Terms'] = terms

        return item
