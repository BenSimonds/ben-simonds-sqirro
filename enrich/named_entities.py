from squirro.sdk import PipeletV1, require

@require('nltk')
class NamedEntities(PipeletV1):
    def consume(self, item):
        """ Takes an item, adds to it and returns item."""
        
      

        def prep_text(self, item):
            """Takes item. Removes code and title from body, returns raw text."""
            item_body = item['body']

            # Remove title. Maybe insert a . after actually...
            item_body.find('h1').extract()

            # Remove code blocks.
            if item_body.find('pre'):
                for codeblock in item_body.find_all('pre'):
                    codeblock.extract()

            return item_body.text

        
        def chunk_text(self, text):
            """Preps text for nltl named entity extraction"""
            sentences = self.nltk.sent_tokenize(text)
            tokenized_sentences = [self.nltk.word_tokenize(s) for s in sentences]
            tagged_sentences  = [self.nltk.pos_tag(sentence) for sentence in tokenized_sentences]
            chunked_sentences = self.nltk.ne_chunk_sents(tagged_sentences, binary=True) # Returns a generator.

            return chunked_sentences

        def find_entities(self, chunked):
            """Performs named entitiy exraction by recursing through the chunked tree structure."""

            def traverse(tree):
                """Recurse through self.nltk tree."""
                entity_names = []

                if hasattr(tree, 'label') and tree.label():
                    if tree.label == 'NE':
                        # Append chunk to entity names, as a single string.
                        entity_names.append(' '.join(child[0] for child in tree))
                    else:
                        for child in tree:
                            entity_names.extend(traverse(child))

                return entity_names

            entity_names = []

            for tree in chunked:
                # We could do a collections.Counter type of thing, but not sure how to use it front end.null=
                # Lets just return a list of unique NEs

                entity_names.extend(traverse(chunked))

            return entity_names


        item_text_chunked = chunk_text(prep_text(item))
        item_named_ents = find_entities(item_text_chunked)

        item['keywords']['Named Entities'] = item_named_ents

        return item
