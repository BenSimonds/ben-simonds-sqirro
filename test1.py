# Testing: See here: https://gist.github.com/onyxfish/322906

import nltk
import numpy
import pprint

pp = pprint.PrettyPrinter(indent=4)

with open('text.txt', 'r') as f:
    sample = f.read()
     
sentences = nltk.sent_tokenize(sample)
tokenized_sentences = [nltk.word_tokenize(sentence) for sentence in sentences]
tagged_sentences = [nltk.pos_tag(sentence) for sentence in tokenized_sentences]
chunked_sentences = nltk.ne_chunk_sents(tagged_sentences, binary=True)

def find_entities(chunks):

	def traverse(tree):
		"""Recurse through nltk tree."""

		entity_names = []

		if hasattr(tree, 'label') and tree.label:
			print(tree.label())
			if tree.label() == 'NE':
				entity_names.append(' '.join(child[0] for child in tree))
			else:
				for child in tree:
					entity_names.extend(traverse(child))
		return entity_names


	named_entities = []

	for chunk in chunks:
		entities = sorted(list([word for tree in chunk for word in traverse(tree)]))

		for e in entities:
			if e not in named_entities:
				named_entities.append(e)

	return named_entities

ne = find_entities(chunked_sentences)

print(ne)

#def extract_entity_names(t):
#    entity_names = []
    
#     if not hasattr(t,'_label'):
#     	for child in t:
#                 entity_names.extend(extract_entity_names(child))
#     else:
#     	pp.pprint(t.__dict__)
#     	print(hasattr(t,'label'))
#     	print(hasattr(t,'_label'))
#     	print(t.label())

#     	if t.label == 'NE':
# 	        entity_names.append(' '.join([child[0] for child in t]))


#     return entity_names            

# 	   #  if t.label == 'NE':
# 	   #      entity_names.append(' '.join([child[0] for child in t]))
# 	   #  else:
# 	   #      for child in t:
# 	   #          entity_names.extend(extract_entity_names(child))
	                
#     # return entity_names



# test = extract_entity_names(chunked_sentences)

# print(test)


# # entity_names = []
# # for tree in chunked_sentences:
# #     # Print results per sentence
# #     #print(extract_entity_names(tree))
# #     #print(tree.label())
    
# #     entity_names.extend(extract_entity_names(tree))

# # Print all entity names
# #print entity_names

# # Print unique entity names
# #print(entity_names)