#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Scrape the Keyrus blog for content that can be loaded into Squirro.
"""

import logging
import requests
import re
from nltk import sent_tokenize, word_tokenize, pos_tag, ne_chunk_sents

from datetime import datetime
from bs4 import BeautifulSoup as bs

from squirro.dataloader.data_source import DataSource

log = logging.getLogger(__name__)


class KeyrusBlogLoader(DataSource):
    """Load data from Keyrus Blog"""

    def __init__(self):
        pass

    def connect(self, inc_column=None, max_inc_value=None):
        """Check connection to website is working"""
        print('Attempting to connect...')
        self.home_url = 'http://blog.keyrus.co.uk/archives.html'
        self._get_page(self.home_url)

    def disconnect(self):
        """Disconnect from the source."""
        pass

    def getDataBatch(self, batch_size):
        """
        Generator - Get data from source on batches.

        :returns a list of dictionaries
        """

        # Get blog posts from archive.
        print("## Getting post archive.")
        archive = self._get_post_archive()
        print(archive)
        for post_url in archive:
            items = []
            items.append(self._get_post_item(post_url))

            yield items

    def getSchema(self):
        """
        Return the schema of the dataset
        :returns a List containing the names of the columns retrieved from the
        source
        """

        schema = [
            'title',
            'body',
            'created_at',
            'link',
            'named_entities',
            'id',
        ]

        return schema

    def getJobId(self):
        """
        Return a unique string for each different select
        :returns a string
        """
        # Generate a stable id that changes with the main parameters

        return "1"

    def add_args(self, parser):
        """
        Add source arguments to the main arguments parser
        """
    #    source_options = parser.add_argument_group('Source Options')

    #    source_options.add_argument('--first-custom-param',
    #                             help="Custom Dataloader Plugin Argument 1")
    #    source_options.add_argument('--second-custom-param',
    #                            help='Custom Dataloader Plugin Argument 2')

        return parser

    def _get_post_item(self, url):
        """scrape post and return item to load with data loader."""
        # print('Gettting Post Item: ' + url)

        post = self._get_page(url)

        # Get the data we need.
        title = post.title.text
        body = post.find('div', {'class': 'content'})

        # Add title to body so it's nice and promminent.
        tag = post.new_tag('h1')
        tag.string = title
        body.insert(0, tag)

        # Extract comments section
        comments = body.find('div', {'class': 'comments'})
        comments.extract()

        # Created date and authorneeds a regular expression.
        date_ele = post.find('p', {'class': 'header-date'})
        date_parsed = re.search(
            '[a-z]{3} \d\d \w+ \d\d\d\d',
            date_ele.text,
            flags=re.I)
        date_created = date_parsed.group(0)
        date_parsed = datetime.strptime(
            date_created, '%a %d %B %Y').strftime('%Y-%m-%d')

        # Author and category sit inside date_ele as well.
        auth_cat = date_ele.find_all('a')
        author = auth_cat[0].text
        category = auth_cat[1].text

        # Get Named entities with nltk.
        body_prepped = self._prep_body(body)
        body_chunked = self._chunk_text(body_prepped)
        body_named_ents = self._find_entities(body_chunked)

        named_entities = body_named_ents

        item = {
            "title": title,
            "link": url,
            "body": body,
            "created_at": date_parsed,
            "author": author,
            "category": category,
            "named_entities": ';'.join(named_entities),
            "id": url.rsplit('/', 1)[-1]
        }

        return item

    def _get_page(self, url):
        """Get page with requests and return a BeautifulSoup object"""
        print("Getting Page: " + url)
        page = requests.get(url)
        page_soup = bs(page.text, 'html.parser')
        print(page_soup.find('a'))
        return page_soup

    def _get_post_archive(self):
        """Scrape the archive page for the URLs of the
        blog posts and return them in a list."""

        blog_archive_url = 'http://blog.keyrus.co.uk/archives.html'
        print("Getting post archive from: " + blog_archive_url)

        archive = self._get_page(blog_archive_url)

        # Find Urls of articles.
        archive = archive.find('div', {'class': 'archive-container'})
        posts = archive.find('dl').find_all('a')
        posts_urls = [a.get('href') for a in posts]

        return posts_urls

    def _prep_body(self, body):

        # Remove title. Maybe insert a . after actually...
        body.find('h1').extract()

        # Remove code blocks.
        if body.find('pre'):
            for codeblock in body.find_all('pre'):
                codeblock.extract()

        #print('-----BODY TEXT-----\n',body.text)
        return body.text

    def _chunk_text(self, text):
            """Preps text for nltl named entity extraction"""
            sentences = sent_tokenize(text)
            tokenized_sentences = [word_tokenize(s) for s in sentences]
            tagged_sentences  = [pos_tag(sentence) for sentence in tokenized_sentences]
            chunked_sentences = ne_chunk_sents(tagged_sentences, binary=True) # Returns a generator.

            return chunked_sentences

    def _find_entities(self, chunked):
        """Performs named entitiy exraction by recursing through
        the chunked tree structure."""

        def traverse(tree):
            """Recurse through nltk tree."""
            entity_names = []

            #print(type(tree).__name__,tree)

            if type(tree).__name__ == 'Tree':
                if tree.label() == 'NE':
                    # Append chunk to entity names, as a single string.
                    entity_names.append(' '.join(child[0] for child in tree))
                else:
                    for child in tree:
                        entity_names.extend(traverse(child))
            return entity_names

        entity_names = []

        for tree in chunked:
            # We could do a collections.Counter type of thing
            # but not sure how to use it front end.
            # Lets just return a list of unique NEs
            #print(tree, type(tree))

            entity_names.extend(traverse(tree))

        print('Entities Found: ' + ', '.join(entity_names))
        return entity_names
