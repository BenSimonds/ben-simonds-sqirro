#!/bin/bash
set -e
cd "$(dirname "$0")"
source ../../common/config/config.sh

squirro_data_load -v \
    --token $TOKEN \
    --cluster $CLUSTER \
    --project-id $PROJECT_ID \
    --source-script 'keyrus_blog_posts.py' \
    --source-name 'Keyrus Blog Posts' \
    --map-title 'title' \
    --map-body 'body' \
    --map-id 'id' \
    --map-url 'link' \
    --map-created-at 'created_at' \
    --facets-file 'facets.json'
