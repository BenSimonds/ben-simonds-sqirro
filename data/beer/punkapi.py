import requests
import logging
import json

from squirro.dataloader.data_source import DataSource

log = logging.getLogger(__name__)

# Scrape some data from punk api:

api_url = "https://api.punkapi.com/v2/beers"


class BeerLoader(DataSource):
    """Load data from Keyrus Blog"""

    def __init__(self):
        pass

    def connect(self, inc_column=None, max_inc_value=None):
        """Check connection to website is working"""
        pass

    def disconnect(self):
        """Disconnect from the source."""
        pass

    def getJobId(self):
        """
        Return a unique string for each different select
        :returns a string
        """
        # Generate a stable id that changes with the main parameters

        return "1"

    def add_args(self, parser):
        """
        Add source arguments to the main arguments parser
        """
    #    source_options = parser.add_argument_group('Source Options')

    #    source_options.add_argument('--first-custom-param',
    #                             help="Custom Dataloader Plugin Argument 1")
    #    source_options.add_argument('--second-custom-param',
    #                            help='Custom Dataloader Plugin Argument 2')

        return parser

    def getDataBatch(self, batch_size):
        """
        Generator - Get data from source on batches.

        :returns a list of dictionaries
        """

        # Get beers from api:
        for p in range(1,10):
            r = requests.get(api_url, params={'page': p, 'per_page':80})

            data = r.json()

            for x in data:

                items = []

                name = x['name']
                tagline = x['tagline']
                description = x['description']
                first_brewed = x['first_brewed']
                img_url = x['image_url']
                food_parings = x['food_pairing']
                beerid = x['id']
                abv = x['abv']

                item = {
                    'name': name,
                    'tagline': tagline,
                    'description': description,
                    'first_brewed': first_brewed,
                    'img_url': img_url,
                    'food_parings': ';'.join(food_parings),
                    'id': beerid,
                    'abv': abv
                }

                items.append(item)

                yield items

    def getSchema(self):
        """
        Return the schema of the dataset
        :returns a List containing the names of the columns retrieved from the
        source
        """

        schema = [
            'name',
            'tagline',
            'description',
            'first_brewed',
            'img_url',
            'food_parings',
            'id',
            'abv'
        ]

        return schema
