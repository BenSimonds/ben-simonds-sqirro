#!/bin/bash
set -e
cd "$(dirname "$0")"
source ../../common/config/config.sh

squirro_data_load -v \
    --token $TOKEN \
    --cluster $CLUSTER \
    --project-id 'aJjOKLXHQVmmoQD64ryg_A' \
    --source-script 'punkapi.py' \
    --source-name 'Punk API' \
    --map-title 'name' \
    --map-body 'description' \
    --map-id 'id' \
    --map-created-at 'first_brewed' \
    --facets-file 'facets.json' \
    --pipelets-file 'pipelets.json'
