#!/bin/bash
set -e                                           # on error, quit script
cd "$(dirname "$0")"                             # cd to the directory where the file is located, this is important because pipelet files and facet files are relative 
source ../../common/config/config.sh

squirro_data_load -vv \
    --token $TOKEN \
    --cluster $CLUSTER \
    --project-id $PROJECT_ID \
    --source-type excel \
    --source-file 'Webinar Feedback.xlsx' \
    --excel-sheet 'Topic1 Business Expenses Dec 16' \
    --source-name 'Business Expenses for Self-Employed' \
    --map-id 'ID' \
    --map-title 'Would you recommend this webinar service to others? Please give reasons for your score at Q6 below' \
    --map-body 'For questions 4 and 5, please give reasons for your score, and if you are planning to call the helpline, please tell us why' \
    --map-created-at 'Date' \
    --facets-file 'facets.json' \
    --pipelets-file 'pipelets.json'
    
